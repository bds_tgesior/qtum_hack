#!/usr/bin/env bash

if [ -z "${aws_profile}" ]; then
  aws="aws"
else
  aws="aws --profile $aws_profile"
fi

aws_region="$1"
build_vcs_number="$2"
docker_registry_url="$3"

PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." && pwd )"
pushd ${PROJECT_DIR}

email_option=""
eval "${aws} ecr get-login --region ${aws_region} --no-include-email"
if [ $? -eq 0 ]; then
  email_option="--no-include-email"
fi

set -e

command="eval \$(${aws} ecr get-login --region ${aws_region} ${email_option}) && \
  \
  docker build -t www:${build_vcs_number} -f backend/Dockerfile . && \
  docker tag www:${build_vcs_number} ${docker_registry_url}/www:app_${build_vcs_number} && \
  docker push ${docker_registry_url}/www:app_${build_vcs_number} && \
  docker rmi -f ${docker_registry_url}/www:app_${build_vcs_number} && \
  docker rmi -f www:${build_vcs_number}"

eval ${command}

popd
