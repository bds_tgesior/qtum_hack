import {post} from "../apiClient";

export async function start(carId, coverage, cap, rate) {
    const response = await post('/insurance', {carId, coverage, cap, rate})
    return response
}