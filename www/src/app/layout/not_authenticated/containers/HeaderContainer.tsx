import * as React from 'react';
import Nav from "../components/Nav";

export class HeaderContainer extends React.Component {

    render() {
        return (
            <header>
                <Nav/>
            </header>
        )
    }

}