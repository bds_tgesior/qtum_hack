import {NextFunction, Response} from 'express'
import {token} from 'gen-uid'
import * as jwt from 'jsonwebtoken'

import HttpError from './httpError'
import HttpStatus from './httpStatus'
import {ApiRequest} from './metadata'
import {ApiUser} from './auth'

export interface Logger {
    info(...args)
    log(...args)
    warn(...args)
    error(...args)
}

class LoggerImpl implements Logger {
    private _req: ApiRequest
    private _requestImprint: string = ''

    constructor(req?: ApiRequest) {
        if (req) {
            this._req = req

            const imprintParts = [req.requestId]

            if (req.apiClient) {
                imprintParts.push(req.apiClient.device || '')
                imprintParts.push(req.apiClient.platformVersion || '')

                if (req.apiClient.version) {
                    imprintParts.push(`#${req.apiClient.version}`)
                }
                if (req.apiClient.locale) {
                    imprintParts.push(`${req.apiClient.locale} locale`)
                }
            }

            this._requestImprint = imprintParts.filter(x => !!x).join(', ')
        }
    }

    private static _basicUserImprintString(user: ApiUser) {
        return `${(user.role || '').toLowerCase()} ${user.id}`
    }

    private get _userImprint() {
        if (!this._req) {
            return ''
        }

        let tokenPayload = jwt.decode((this._req.get('authorization') || '').replace('JWT ', ''))
        let imprint = `tp: ${JSON.stringify(tokenPayload)}; `

        if (!this._req.user) {
            imprint += 'no user'
        } else {
            imprint += LoggerImpl._basicUserImprintString(this._req.user)
        }

        return imprint
    }

    private _log(level, ...args) {
        return console[level](new Date().toISOString(), level, `[${this._requestImprint}; ${this._userImprint}]`, ...args)
    }

    info(...args) {
        return this._log('info', ...args)
    }

    log(...args) {
        return this._log('log', ...args)
    }

    warn(...args) {
        return this._log('warn', ...args)
    }

    error(...args) {
        return this._log('error', ...args)
    }

    runInContext<T>(context: ApiRequest, action: (Logger) => T) {
        return action(new LoggerImpl(context))
    }
}

export const logger = new LoggerImpl()

function getLoggerForStatusCode(logger: Logger, statusCode: number) {
    if (statusCode >= HttpStatus.serverError) {
        return logger.error.bind(logger)
    }
    if (statusCode >= HttpStatus.badRequest) {
        return logger.warn.bind(logger)
    }

    return logger.log.bind(logger)
}

export function bindLogger(req: ApiRequest, res: Response, next: NextFunction) {
    const cleanup = () => {
        res.removeListener('finish', logFn)
        res.removeListener('close', abortFn)
        res.removeListener('error', errorFn)
    }

    const logFn = () => {
        cleanup()
        let logger = getLoggerForStatusCode(req.logger, res.statusCode)
        logger(`${res.statusCode} ${res.statusMessage}; ${res.get('Content-Length') || 0}b sent`)
    }

    const abortFn = () => {
        cleanup()
        let logger = getLoggerForStatusCode(req.logger, HttpStatus.badRequest)
        logger('Request aborted by the client')
    }

    const errorFn = err => {
        cleanup()
        let logger = getLoggerForStatusCode(req.logger, HttpStatus.serverError)
        logger(`Request pipeline error: ${err}`)
    }

    logger.runInContext(req, contextualLogger => {
        req.logger = contextualLogger
        req.logger.info(`${req.method} ${req.originalUrl}`)

        res.on('finish', logFn) // successful pipeline (regardless of its response)
        res.on('close', abortFn) // aborted pipeline
        res.on('error', errorFn) // pipeline internal error

        next()
    })
}

export function expressError(err: HttpError, req: ApiRequest, res: Response, next: NextFunction) {
    if (err) {
        console.log(err)
        let statusCode = err.statusCode || HttpStatus.serverError
        getLoggerForStatusCode(req.logger || logger, statusCode)(err)

        if (err.jsonResponse) {
            res.status(statusCode).send(err.jsonResponse)
        } else {
            res.status(statusCode).send()
        }
    }
}
