import {NextFunction, Request, Response} from 'express'
import {token} from 'gen-uid'
import * as _ from 'lodash'

import {ApiUser} from './auth'
import {Logger} from './logger'

interface RequestApiClient {
    version?: string
    platformVersion?: string
    device?: string
    locale?: string
}

export interface ApiRequest extends Request {
    requestId: string
    apiVersion: number
    apiClient: RequestApiClient
    logger: Logger

    setLocale: (locale: string) => void
    __: (key: string, ...args: any[]) => string

    bodyFor: <T>(attrs: { new({}): T }) => _.PartialDeep<T>

    user?: ApiUser
}

function parseApiVersion(req: Request) {
    let match = req.path.match(/^\/api\/v([0-9]+)\//i)
    return (match && parseInt(match[1], 10)) || 1
}

function bodyFor<T extends object>(attrs: { new({}): T }): _.PartialDeep<T> {
    const validatedData = new attrs(this.body)
    return _.pick(validatedData, Object.keys(this.body))
}

export function enhanceRequest(req: ApiRequest, res: Response, next: NextFunction) {
    req.requestId = req.requestId || token(true).substr(0, 8)
    req.apiVersion = parseApiVersion(req)

    req.apiClient = {
        version: req.get('X-ClientVersion'),
        platformVersion: req.get('X-ClientPlatformVersion'),
        device: req.get('X-ClientDevice'),
        locale: req.get('X-ClientLocale')
    }

    if (req.apiClient.locale) {
        req.setLocale(req.apiClient.locale.split(/[_-]/)[0].toLowerCase())
    }

    req.bodyFor = bodyFor.bind(req)
    next()
}

