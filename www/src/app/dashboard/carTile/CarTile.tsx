import * as React from 'react'
import './CarTile.css'
import {Car} from '../../car/carApi'

//const car1Image = require('./../../../images/Audi_Logo.png')

interface Props extends Car {
    onLiveDataClicked: (carId: string) => void
    onBuyInsurance: (carId: string) => void
    isForActiveCar?: boolean
}


export const CarTile = ({carId, carModel, carCompany, vin, productionYear, onLiveDataClicked, onBuyInsurance, isForActiveCar, insurance}: Props) => {

    return (
        <div className="card mb-4 box-shadow mt-5">
            <div className="card-header">
                <h4 className="my-0 font-weight-normal text-center">{carCompany} {carModel}</h4>
            </div>
            <div className="card-body column">
                <h1 className="card-title pricing-card-title text-center"><small className="text-muted"> VIN - </small> {vin} </h1>
                <div className="row justify-content-center">
                    <div className="my-3 p-3 bg-white rounded box-shadow">
                        <h6 className="border-bottom border-gray pb-2 mb-0">car informations</h6>
                        <div className="media text-muted pt-3">
                            <p className="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                                <strong className="d-block text-gray-dark">@ {carCompany} </strong>
                                Car company of this car.
                            </p>
                        </div>
                        <div className="media text-muted pt-3">
                            <p className="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                                <strong className="d-block text-gray-dark text-uppercase">@ {carModel}</strong>
                                Car model of this car.
                            </p>
                        </div>
                        <div className="media text-muted pt-3">
                            <p className="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                                <strong className="d-block text-gray-dark">@ {productionYear}</strong>
                                production year of this car.
                            </p>
                        </div>
                        <div className="media text-muted pt-3">
                            <p className="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                                <strong className="d-block text-gray-dark">@ {carId}</strong>
                                Car identifier number stored in database and on QTUM blockchain network.
                            </p>
                        </div>
                        <small className="d-block text-right mt-3">
                            <a href="#">secured by QTUM blockchain</a>
                        </small>
                    </div>
                    {!isForActiveCar && <div className="col-md-10">
                        <div className="form-group row justify-content-center pt-2 pb-2">
                            <button onClick={()=>onLiveDataClicked(carId)}
                                    className="btn light-orange-bg text-center text-white">Show Live Data >
                            </button>
                        </div>
                    </div>}

                    <div className={"justify-content-center"}>
                        {insurance ? <label>{insurance.coverage.toUpperCase()} insurance active</label> : <button type="button" onClick={()=>onBuyInsurance(carId)}
                                                                                                                  className="btn btn-lg btn-block btn-theme mt-2">Purchase insurance ></button>}
                    </div>
                </div>
            </div>
        </div>

//         <div className = "card-width card-padding">
//         <div className={`card ${isForActiveCar? "fadeIn": "fadeInUp"}`}>
// <img className="card-img-top" src={car1Image} alt="Card image cap"/>
//         <ul className="list-group list-group-flush">
//         <li className="list-group-item">Car registration id - {carId}</li>
//     <li className="list-group-item">Car company - {carCompany} </li>
//     <li className="list-group-item">Production year - {productionYear}</li>
// </ul>
//     <div className="card-body">
//         <a href="#" className="card-link" onClick={()=>onBuyInsurance(carId)}>Buy insurance</a>
//         <a href="#" className="card-link" onClick={()=>onLiveDataClicked(carId)}>Live data</a>
//     </div>
// </div>
// </div>
    )
}