import * as React from 'react';
import PageTitle from "../../layout/not_authenticated/components/PageTitle";
import CarRegisterForm from "../components/CarRegisterForm";

interface ICarRegisterPageProps {

}

interface ICarRegisterPageStage {

}

const qtumLogo = require('./../../../images/qtum_brand_logo.svg')

export default class CarRegisterPage extends React.Component<ICarRegisterPageProps, ICarRegisterPageStage> {

    constructor(props) {
        super(props)
    }

    render() {

        return (
            <div className="row justify-content-center">
                <div className="col-md-12 ">
                    <PageTitle title={"Car register"} subTitle={"Register car with qtum blockchain service."}
                               img={qtumLogo}/>
                    <CarRegisterForm/>
                </div>
            </div>
        )
    }
}