import * as React from 'react';

interface IPageTitleProps {
    title: string,
    subTitle: string,
    img: any
}

const PageTitle: React.SFC<IPageTitleProps> = (props) => {

    return (
        <div className="d-flex justify-content-md-center align-items-center p-1 my-3 text-white-50 light-blue-bg rounded shadow-sm">
            <img className="mr-2" src={props.img} alt="qtum_logo" width="60"/>
            <div className="lh-100">
                <h6 className="mb-0 text-white lh-100">{props.title}</h6>
                <small>{props.subTitle}</small>
            </div>
        </div>
    )

}

export default PageTitle