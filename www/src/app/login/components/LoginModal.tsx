import * as React from 'react';
import * as Modal from 'react-modal';
import LoginForm from "./LoginForm";
import {Link} from 'react-router-dom';

interface ILoginModalProps {
    isOpen: boolean,
    hideModal: () => void
}

const customStyles = {
    content: {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)',
        width: '50%',
        height: 'auto'
    }
};

const LoginModal: React.SFC<ILoginModalProps> = (props) => {

    return (
        <Modal
            isOpen={props.isOpen}
            onRequestClose={props.hideModal}
            style={customStyles}
        >
            <div className="container-fluid justify-content-center">

                <div className="row justify-content-center">
                    <h4 className="font-weight-bold pt-5">LOG IN</h4>
                </div>
                <div className="row justify-content-center pt-4">
                    <div className="col-md-10">
                        <LoginForm/>
                        <p className="text-center">No account? U can create one
                            <Link to="/register" onClick={props.hideModal}
                                  className="light-orange"> here</Link>.
                        </p>
                    </div>
                </div>
            </div>
        </Modal>
    )

}

export default LoginModal