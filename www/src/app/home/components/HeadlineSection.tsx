import * as React from 'react';

import './HeadlineSection.css';

interface IHeadlineSection {
    primaryTitle: string,
    secondaryTitle: string
}

const HeadlineSection: React.SFC<IHeadlineSection> = (props) => {

    return <React.Fragment>
        <div className="d-md-flex flex-md-equal w-100 my-md-3 pl-md-3">
            <div className="bg-dark mr-md-3 pt-3 px-3 pt-md-5 px-md-5 text-center text-white w-50 overflow-hidden ">
                <div className="my-3 py-3">
                    <h2 className="display-5">{props.primaryTitle}</h2>
                    <p className="lead">And an even wittier subheading.</p>
                </div>
                <div className="bg-light shadow-sm mx-auto frame-background"/>
            </div>
            <div className="bg-light mr-md-3 pt-3 px-3 pt-md-5 px-md-5 text-center w-50 overflow-hidden">
                <div className="my-3 p-3">
                    <h2 className="display-5">{props.secondaryTitle}</h2>
                    <p className="lead">And an even wittier subheading.</p>
                </div>
                <div className="bg-dark shadow-sm mx-auto frame-background"/>
            </div>
        </div>
    </React.Fragment>

}

export default HeadlineSection