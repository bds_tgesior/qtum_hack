import * as React from 'react'
import {
    withRouter
} from 'react-router-dom'
import {login} from './../authApi'
import {apiTokenStore, apiUserStore} from "../../apiStore"
import 'react-block-ui/style.css'

interface ILoginFormProps {
    history: any
}

class LoginFormState {
    email: string = ''
    password: string = ''
    error: boolean = false
    submitted: boolean = false
}

class LoginForm extends React.Component<ILoginFormProps, LoginFormState> {

    constructor(props) {
        super(props)
        this.state = new LoginFormState()
    }

    handleInputChange(event) {
        const target = event.target
        const value = target.value
        const name = target.name

        this.setState({
            ...this.state,
            [name]: value
        })
    }

    async handleSubmit(e) {
        e.preventDefault()

        try {
            this.setState({submitted: true})
            const result = await login(this.state.email, this.state.password)

            apiTokenStore.set(result.apiToken)
            apiUserStore.set(result.user)

            this.props.history.push('/dashboard')
            this.setState({submitted: false})

        } catch (err) {
            this.setState({error: true, submitted: false})
        }

    }

    render() {

        let error = ''

        if (this.state.error) {
            error = 'Invalid login or password.'
        }

        return (
            <React.Fragment>
                <form onSubmit={this.handleSubmit.bind(this)}>
                    <div className="form-group row justify-content-center">
                        <input type="email" name="email" value={this.state.email} placeholder="Email" required
                               className="form-control w-75" onChange={this.handleInputChange.bind(this)}/>
                    </div>
                    <div className="form-group row justify-content-center mt-1">
                        <input type="password" name="password" value={this.state.password} required
                               placeholder="Password"
                               className="form-control w-75" onChange={this.handleInputChange.bind(this)}/>
                    </div>
                    <div className="form-group row justify-content-center pt-5 pb-5">
                        <input className="btn btn-theme w-25" type="submit" value="Log in"/>
                    </div>
                    <div className="form-group row justify-content-center">
                        <p className="text-danger">{error}</p>
                    </div>
                </form>
            </React.Fragment>

        )
    }

}

export default withRouter(LoginForm)