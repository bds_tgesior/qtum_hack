import {post} from '../apiClient';
import {ApiUser} from '../authClient';

export async function login(email: string, password: string): Promise<{ apiToken: string, user: ApiUser }> {
    return (await post<{ apiToken, user }>(`/user/auth`, {email, password}))
}
