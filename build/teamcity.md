## TeamCity setup

1. Configure the subdomain in CloudFlare
1. Prepare docker-compose.yml file and commit it into dockerfiles repo
1. Run rancher stack
1. Upload JDBC drivers
    - `cd /media/data32/var/lib/teamcity-X-server/lib/jdbc`
    - `wget https://jdbc.postgresql.org/download/postgresql-42.2.2.jar`
1. Go to ci-X.brightinventions.pl and configure database
    - host is `database:5432`, database empty, username `postgres`, password from stack config
1. Create superuser account + other users account
1. Disable creating accounts: Administration > Authentication > Built-in > Edit > Allow user registration from the login page
1. Plugins list > upload node plugin from https://teamcity.jetbrains.com/viewType.html?buildTypeId=bt434 + restart server
    - we used version 2.0.7 successfully, but couldn't run 2.0.10 due to internal exception in the plugin
1. Authorize build agent(s)
1. Import project config
    - modify files in `teamcity-config` to replace all TODO's with the project name
    - ensure `teamcity-config/version.txt` matches your TeamCity server instance; they claim they only accept imports from exactly the same versions but apparently it works from older ones, too. Just lie to them it's a new one :)
    - run `./build-teamcity-config.sh` from within `teamcity-config` directory
    - go to Server Administration > Projects Import > upload and run `teamcity-config.zip` created
1. Upload SSH key for TeamCity, fix VCS roots to use this key
1. Upload ssh-access key to be able to deploy to AWS
1. Set AWS credentials to parameters, db passwords
