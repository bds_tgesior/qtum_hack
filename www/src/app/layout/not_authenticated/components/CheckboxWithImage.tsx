import * as React from 'react';
import './CheckboxWithImage.css';

interface ICheckboxWithImage {
    img: any,
    alt: string,
    value: string,
    name: string,
    active?: boolean,
    checked: boolean,
    onClick: () => void
}

const CheckboxWithImage: React.SFC<ICheckboxWithImage> = (props) => {

    return (

        <div className={props.checked ? "col-md-3" : "col-md-2"}><label className="btn"><img
            src={props.img} width="100px"
            alt={props.alt} className="icon-img img-thumbnail img-check p-4"/>
            <input type="checkbox" onClick={props.onClick} name={props.name} id="item4" value={props.value} className="d-print-none icon-input"
                   autoComplete="off" /></label>
        </div>

    )
}

export default CheckboxWithImage