import * as React from 'react';
import './Footer.css';

const Footer:React.SFC = () => {

    return <React.Fragment>
        <footer className="main-footer footer">
            <div className="container-fluid">
                <div className="row light-blue-bg">
                    <div className="col-sm-6">
                        <p className="text-white">Mondays Insurance © 2019</p>
                    </div>
                    <div className="col-sm-6 text-right">
                        <p className="text-white">Created for <a className="light-orange" href="https://hackathon.qtum.org/en">QTUM Hackathon</a></p>
                    </div>
                </div>
            </div>
        </footer>
    </React.Fragment>

}

export default Footer