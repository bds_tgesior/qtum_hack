import {post} from "../apiClient";

interface User {
    firstName: string
    lastName: string
    email: string
    password: string
}

export async function register(user: User) {
    const response = await post('/register', user)
    return response
}