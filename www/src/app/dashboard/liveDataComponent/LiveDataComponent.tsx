import * as React from 'react'
import './LiveDataComponent.css'
import {AreaChart} from 'react-easy-chart'
import * as socketIOClient from "socket.io-client"

interface LineChartData {
    x: number
    y: number
}

interface Props {
    carId: string
    onCloseButtonClicked: () => void
    isLiveBoardActive: boolean
    coverage: string
    cap: number
    rate: number
}

interface State {
    speed: Array<LineChartData>
    mileage: Array<LineChartData>
    kilometersToNextService: Array<LineChartData>
    tires: number
    temperature: number
}

export class LiveData extends React.Component<Props, State> {
    constructor(props) {
        super(props)
        this.state = {
            speed: [{x: 1, y: 0}],
            mileage: [{x: 1, y: 0}],
            kilometersToNextService: [{x: 1, y: 0}],
            tires: 0,
            temperature: 0
        }
    }

    private setStateArray = (prevState, stateKey, data, dataKeyToExtract): any => {
        if (prevState[stateKey] && prevState[stateKey].length < 15) {
            const lastEntry = prevState[stateKey][prevState[stateKey].length - 1]
            return {
                [stateKey]: [
                    ...prevState[stateKey],
                    {x: lastEntry ? lastEntry.x + 1 : 2, y: parseFloat(data[dataKeyToExtract])}]
            }
        } else {
            const lastEntry = prevState[stateKey] ? prevState[stateKey][prevState[stateKey].length - 1] : 0
            return {
                [stateKey]: [
                    lastEntry,
                    {x: lastEntry ? lastEntry.x + 1 : 2, y: parseFloat(data[dataKeyToExtract])}]
            }
        }
    }

    private renderInfoCard = (title, data) => {
        return (<div className="card w-75">
            <div className="card-header">
                {title}
            </div>
            <div className="card-body">
                {data}
            </div>
        </div>)
    }

    componentDidMount() {
        const socket = socketIOClient('http://localhost:3001')
        socket.on('diagnostics', (data) => {
            this.setState((prevState) => {
                    return this.setStateArray(prevState, 'speed', data, 'speed')
                }
            )
            this.setState((prevState) => {
                    return this.setStateArray(prevState, 'mileage', data, 'mileage')
                }
            )
            this.setState({tires: data.tires})
            this.setState({temperature: data.temperature})
        })
        socket.on('maintenance', (data) => {
            this.setState((prevState) => {
                    return this.setStateArray(prevState, 'kilometersToNextService', data, 'kilometersToNextService')
                }
            )

        })
    }

    render() {
        const {isLiveBoardActive, onCloseButtonClicked, coverage, cap, rate} = this.props
        const lineChartStateObject = {
            Speed: this.state.speed,
            Mileage: this.state.mileage,
            NextServiceKm: this.state.kilometersToNextService
        }
        const lineChartYAxis = {Speed: 'km/h', Mileage: 'km', NextServiceKm: 'km'}
        const currentMillage = this.state.mileage[this.state.mileage.length - 1].y || 0
        return (
            <div className={`col-md-6  ${isLiveBoardActive ? "slideInRight" : ""} `}>
                <div className="card-deck live-card-padding light-blue-bg ml-2 mr-2">
                    {coverage &&
                    this.renderInfoCard('Coverage and rate', `Coverage ${coverage}, ${rate} euro per km`)}
                    {cap && this.renderInfoCard('Insurance cap', `${cap} euro`)}
                    {rate && this.renderInfoCard('Insurance total cost', `${(currentMillage * rate /100).toFixed(2)} euro`)}
                </div>
                <div className="card-deck live-card-padding">
                    {Object.keys(lineChartStateObject).map(objectKey => <div key={objectKey} className="card">
                        <div className="card-header">
                            {objectKey}
                        </div>
                        <div className="card-body">
                            <AreaChart
                                margin={{top: 10, right: 10, bottom: 50, left: 50}}
                                axisLabels={{x: lineChartYAxis[objectKey], y: 'Time'}}
                                axes
                                grid
                                width={200}
                                height={200}
                                data={[
                                    lineChartStateObject[objectKey]
                                ]}
                            ></AreaChart>
                        </div>
                    </div>)}
                </div>
                <div className={"card-deck live-card-padding"}>
                    <div className="card">
                        <div className="card-header">
                            Tires endurance (%)
                        </div>
                        <div className="card-body">
                            {this.state.tires.toFixed(3)}
                        </div>
                    </div>
                    <div className="card">
                        <div className="card-header">
                            Engine temperature
                        </div>
                        <div className="card-body">
                            {this.state.temperature.toFixed(3)}
                        </div>
                    </div>
                    <div className="card">
                        <div className={"text-center button-padding "}>
                            <button onClick={() => onCloseButtonClicked()}
                                    className="btn btn-theme">Close
                            </button>
                        </div>
                    </div>
                </div>

            </div>
        )
    }
}