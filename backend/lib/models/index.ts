import {sync} from "../../config"
import * as Sequelize from "sequelize"

declare module 'sequelize' {
    interface Instance<TAttributes> {
        dataValues: TAttributes
    }
    interface Model<TInstance, TAttributes> {
        prototype: TInstance
        create(values?: Partial<TAttributes>, options?: Sequelize.CreateOptions): Promise<TInstance>
    }
}

const sequelize = new Sequelize(sync().database, {
    define: {
        underscored: true
    },
    logging: false
})

const models = {
    sequelize,

    // User: User(sequelize),
    User: <Sequelize.Model<any, any>> {} // TODO replace with proper entities
    // ...
}

export type Models = typeof models
export default models

export interface HasTimestamps {
    created_at: Date
    updated_at: Date
}