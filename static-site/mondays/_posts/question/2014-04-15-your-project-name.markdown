---
layout: question
title:  "What does Mondays do?"
date:   2018-09-16 16:54:46
author: mountbranch
categories:
- question
---
Mondays is a project in the QTUM virtual hackathon submitted by a group of blockchain developers, architects, enthusiasts. We decided to learn more about QTUM and build something that we thought could have a real impact.

It provides a dynamic, on-demand car liability insurance based on the QTUM blockchain.