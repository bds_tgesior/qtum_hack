import {ApiRequest} from "../../metadata";
import * as Express from "express";
import HighMobility from "../../../lib/highMobility/api";

export async function climate(req: ApiRequest, res: Express.Response, next: Express.NextFunction) {
    try {
        const HM = await HighMobility.create()

        const getClimateResponse = await HM.getClimate()
        res.send({getClimateResponse})
    } catch(err) {
        console.log(err)
        next(err)
    }
}