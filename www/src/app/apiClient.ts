import 'whatwg-fetch'
import * as querystring from "querystring"
import {apiTokenStore} from "./apiStore";

const BaseUrl = 'http://localhost:3001/api/v1'

function authHeaders() {
    return {
        'Authorization': apiTokenStore.get() || ''
    }
}

function jsonHeaders(): HeadersInit {
    return new Headers(Object.assign({
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'mode': 'no-cors'
    }, authHeaders()))
}

async function fetch(url: string, requestInit): Promise<Response> {
    const response = await window.fetch(BaseUrl + url, requestInit)

    if (response.status < 200 || response.status >= 300) {
        throw response
    }

    return response
}

type HttpMethod = 'GET' | 'HEAD' | 'POST' | 'PUT' | 'PATCH' | 'DELETE'

async function fetchAndUnwrap<T>(method: HttpMethod, url: string, body?: any): Promise<T> {
    const response = await fetch(url, {
        method,
        headers: jsonHeaders(),
        body: body ? JSON.stringify(body) : undefined
    })

    if (response.status === 204) {
        return {} as T
    }

    return response.json() as Promise<T>
}

export async function get<T>(url: string): Promise<T> {
    return fetchAndUnwrap<T>('GET', url)
}

export async function deleteRequest(url: string, body: any): Promise<void> {
    await fetch(url, {
        method: 'DELETE',
        headers: jsonHeaders(),
        body: JSON.stringify(body)
    })
}

export function post<T>(url: string, body: any): Promise<T> {
    return fetchAndUnwrap<T>('POST', url, body)
}

export function patch<T>(url: string, body: any): Promise<T> {
    return fetchAndUnwrap<T>('PATCH', url, body)
}

export async function download(url: string, body: any): Promise<void> {
    const requestInit = {
        method: 'POST',
        headers: jsonHeaders(),
        body: JSON.stringify(body)
    }

    const response = await window.fetch(url, requestInit)
    if (response.status !== 201) {
        throw response
    }

    let location = response.headers.get('location')
    if (!location) {
        throw new Error('File Location header missing.')
    }
    window.location.href = location
}

export async function upload<T>(url: string, data: any): Promise<T> {
    const formData = new FormData()
    for (let key of Object.keys(data)) {
        let value = data[key]
        if (value && value.constructor && value.constructor.name === 'Object') {
            value = JSON.stringify(value)
        }

        formData.append(key, value)
    }

    const requestInit = {
        method: 'POST',
        headers: authHeaders(),
        body: formData
    }

    const response = await fetch(url, requestInit)
    return response.json() as Promise<T>
}

export async function exportRequest<T>(body: { fileName: string, columns: any }, url: string, customParams: T | {} = {}): Promise<void> {
    const params = Object.assign({_offset: 0, _limit: 0}, customParams)
    await post<void>(`${url}/export?${querystring.stringify(params)}`, body)
}