import {ApiRequest} from "../../metadata";
import * as Express from "express";
import HighMobility from "../../../lib/highMobility/api";


export async function charging(req: ApiRequest, res: Express.Response, next: Express.NextFunction) {
    try {
        const HM = await HighMobility.create()

        const getChargingResponse = await HM.getCharging()
        res.send({getChargingResponse})
    } catch(err) {
        console.log(err)
        next(err)
    }
}