import * as React from 'react';
import './CarRegisterForm.css';
import CheckboxWithImage from "../../layout/not_authenticated/components/CheckboxWithImage";
import {
    withRouter
} from 'react-router-dom';
import {register} from "../carApi";

interface ICarRegisterFormProps {
    history: any
}

class CarRegisterFormState {
    toggleNextStep: boolean = false
    vin?: string = ''
    carCompany: string = ''
    carModel: string = ''
    mileage: number | undefined = 0
    productionYear: number | undefined
    checkedType: string = 'casualCar'
}

const casualCar = require('./../../../images/casual_car.svg')
const sportCar = require('./../../../images/sport_car.svg')
const bus = require('./../../../images/bus.svg')

class CarRegisterForm extends React.Component<ICarRegisterFormProps, CarRegisterFormState> {

    constructor(props) {
        super(props)
        this.state = new CarRegisterFormState()
    }

    handleInputChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;

        this.setState({
            ...this.state,
            [name]: value
        });
    }

    async handleSubmit(e) {
        e.preventDefault()

        try {
            await register(
                {
                    vin: this.state.vin,
                    carCompany: this.state.carCompany,
                    carModel: this.state.carModel,
                    mileage: this.state.mileage,
                    productionYear: this.state.productionYear
                }
            )
        } catch (err) {
            console.log(err)
        }

        this.props.history.push('/dashboard');
    }

    toggleNextStep() {
        this.setState({toggleNextStep: true})
    }

    generateSelectOptions(): HTMLOptionElement[] {
        let options: any[] = []

        for (let i = 1; i < 6; i++) {
            options.push(<option key={i} value={i}>{i}</option>)
        }

        return options
    }

    render() {

        return <React.Fragment>
            <div className="row justify-content-center">
                <div className=" col-md-10 bg-white form-container">
                    <form onSubmit={this.handleSubmit.bind(this)}>
                        <div className={this.state.toggleNextStep ? "d-none" : "form-group mt-5"}>
                            <h5 className="text-center">Hello! Let's register first car with our cool insurance
                                package!</h5>
                            <h1 className={"text-center pt-1"}>Please, tell us more about your car.</h1>
                            <p className="h6 text-center font-weight-bold pt-5">Select your car type:</p>
                            <div className="row justify-content-center mt-4">

                                <CheckboxWithImage checked={this.state.checkedType === "casualCar"} img={casualCar} alt={'casualCar'} value={'casualCar'}
                                                   name={'casualCar'} onClick={() => this.setState({checkedType: 'casualCar'})}/>
                                <CheckboxWithImage checked={this.state.checkedType === "sportCar"} img={sportCar} alt={'sportCar'} value={'sportCar'}
                                                   name={'sportCar'} onClick={() => this.setState({checkedType: 'sportCar'})}/>
                                <CheckboxWithImage checked={this.state.checkedType === "bus"} img={bus} alt={'bus'}
                                                   value={'bus'} name={'bus'}  onClick={() => this.setState({checkedType: 'bus'})}/>
                            </div>
                            <div className="row justify-content-center mt-5">
                                <div
                                    onClick={this.toggleNextStep.bind(this)}
                                    className="btn btn-theme mb-5">Go to next step >
                                </div>
                            </div>
                        </div>
                        {/* Register car form - second step*/}
                        <div className={this.state.toggleNextStep ? "form-group mt-5" : "d-none"}>
                            <div className=" justify-content-center">
                                <h5 className="text-center">Hello! Let's register first car with our cool insurance
                                    package!</h5>
                                <h1 className={"text-center pt-1"}>We are almost done, just few more infos.</h1>
                            </div>
                            <label className="row justify-content-center pt-5" htmlFor="register_firstName">Car
                                company</label>
                            <div className="form-group row justify-content-center">
                                <input id="carCompany" name="carCompany" type="text" className="form-control w-50"
                                       value={this.state.carCompany}
                                       onChange={this.handleInputChange.bind(this)}/>
                            </div>
                            <label className="row justify-content-center" htmlFor="register_lastName">Car model</label>
                            <div className="form-group row justify-content-center">
                                <input id="carModel" name="carModel" type="text" className="form-control w-50"
                                       value={this.state.carModel}
                                       onChange={this.handleInputChange.bind(this)}/>
                            </div>
                            <label className="row justify-content-center" htmlFor="vin">No. VIN</label>
                            <div className="form-group row justify-content-center">
                                <input id="vin" name="vin" type="text" className="form-control w-50"
                                       value={this.state.vin}
                                       onChange={this.handleInputChange.bind(this)}/>
                            </div>

                            <label className="row justify-content-center" htmlFor="register_email">Production
                                year</label>
                            <div className="form-group row justify-content-center">
                                <input id="productionYear" name="productionYear" type="number"
                                       className="form-control w-50" value={this.state.productionYear}
                                       onChange={this.handleInputChange.bind(this)}/>
                            </div>

                            <label className="row justify-content-center" htmlFor="mileage">Car
                                mileage</label>
                            <div className="form-group row justify-content-center">
                                <input id="mileage" name="mileage" type="number"
                                       className="form-control w-50" value={this.state.mileage}
                                       onChange={this.handleInputChange.bind(this)}/>
                            </div>
                            <div className="form-group row justify-content-center pt-5 pb-5">
                                <input className="btn btn-theme" type="submit" value="Register new car >"/>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </React.Fragment>
    }
}

export default withRouter(CarRegisterForm);