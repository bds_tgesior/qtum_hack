import * as bcrypt from 'bcrypt'

export interface AuthorizableMemberAttributes {
    id: string
    email: string
    password: string
    salt: string
    passwordResetToken?: string
    passwordResetSentAt?: Date
}

export interface AuthorizableInstance extends AuthorizableMemberAttributes {
    requestPasswordResetToken(): Promise<string>
    resetPassword(newPassword: string): Promise<void>
}

export async function createHashFor(password: string, salt: string) {
    return new Promise((resolve, reject) => bcrypt.hash(password, salt, function (err, hash) {
        if (err) {
            return reject(err);
        }
        resolve(hash);
    }));
}

export async function createSaltedPassword(user) {
    user.email = user.email.toLowerCase();
    user.salt = await new Promise((resolve, reject) => bcrypt.genSalt(10, function (err, salt) {
        if (err) {
            return reject(err);
        }
        resolve(salt);
    }));

    user.password = await createHashFor(user.password, user.salt)
}
