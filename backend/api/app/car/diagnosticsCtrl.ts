import {ApiRequest} from "../../metadata";
import * as Express from "express";
import HighMobility from "../../../lib/highMobility/api";

export async function diagnostics(req: ApiRequest, res: Express.Response, next: Express.NextFunction) {
    try {
        const HM = await HighMobility.create()

        const getDiagnosticsResponse = await HM.getDiagnostics()
        res.send({getDiagnosticsResponse})
    } catch(err) {
        console.log(err)
        next(err)
    }
}