pragma solidity ^0.4.23;

contract CarLiabilityInsurance {

    // Address where insurance fund is collected
    address private _wallet;
    // Owner account
    address _owner;

    enum Coverages {Basic, Silver, Gold}

    struct Car {
        uint256 modelYear;
        string company;
        string modelName;
        string vin;
        bytes32 carID;
        uint256 mileage;
    }

    struct Insurance{
        Coverages coverage;//Basic covers just property and medical costs ensuing an accident, Silver
        // additionally covers theft and Gold covers weather or catastrophy induced damages as well
        uint256 riskLevel; //between 0 and 99 multiple depending on risky behaviour, age, previous accidents, etc.
        uint256 premium; //updated according to mileage
        uint256 cap; //maximum coverage, given by user
        bool active; //if insurance has been approved and activated by insurer
    }

    mapping(address=>Car) public carOwnership; //address of owner should be connected to the car
    mapping(bytes32=>Car) public cars; //carID maps to a car
    mapping(bytes32=>Insurance) public carInsurance; //carID should be connected to an insurance

    event InsurancePolicyAuthorized(bytes32);
    event InsurancePolicyStarted(bytes32);

    modifier onlyOwnCar(bytes32 _carID)
    {
        require(
            _carID == carOwnership[msg.sender].carID,
            "Sender not authorized."
        );
        // Do not forget the "_;"! It will
        // be replaced by the actual function
        // body when the modifier is used.
        _;
    }

    modifier onlyRegisteredCar(bytes32 _carID) {
        require(
            keccak256(abi.encodePacked(cars[_carID].vin)) != keccak256(''),
            "Car not registered."
        );
        _;
    }

    modifier onlyOwner() {
        require(
            msg.sender == _owner,
            "not owner."
        );
        _;
    }

    constructor(address _fundWallet) public {
        _owner = msg.sender;
        _wallet = _fundWallet;
    }

    function registerCar(uint256 _modelYear, string _company, string _modelName, string _vin, uint256 _mileage) public {
            bytes32 _carID = keccak256(abi.encodePacked(_vin));
            carOwnership[msg.sender] = Car(_modelYear, _company, _modelName, _vin, _carID, _mileage);
        }

    //_coverage is 0, 1 or 2 for basic, silver or gold coverage
    // car must be registered to be able to start an insurance for it
    // risk level is evaluated based on age of driver and other factors, done off-chain. Since it needs to be added by
    // the app and not by the insured herself, insurance is started in a multi-sig manner.
    function startInsuranceUser(bytes32 _carID, uint8 _coverage, uint256 _cap) onlyOwnCar(_carID) public {
        require(uint(Coverages.Gold) >= _coverage);
        carInsurance[_carID] = Insurance(Coverages(_coverage), 0, 0, _cap, false);
        emit InsurancePolicyAuthorized(_carID);
    }

    function startInsuranceInsurer(bytes32 _carID, uint256 _riskLevel, uint256 _premium) onlyOwner onlyRegisteredCar(_carID) public {
        carInsurance[_carID] = Insurance(carInsurance[_carID].coverage, _riskLevel, _premium, carInsurance[_carID].cap, true);
        emit InsurancePolicyStarted(_carID);
    }

    function updateMileage(bytes32 _carID, uint256 _mileage) onlyOwnCar(_carID) public returns(bool){
        if (cars[_carID].mileage > _mileage) {
            cars[_carID].mileage = _mileage;
            //TODO: emit event for mileage increase
            return true;
        }
        return false;
    }

    // function updateInsuranceTerms() onlyOwnCar(_carID) {
    //
    // }
}