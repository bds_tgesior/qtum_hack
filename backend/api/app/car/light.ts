import {ApiRequest} from "../../metadata";
import * as Express from "express";
import HighMobility from "../../../lib/highMobility/api";
import {io} from "../../../index";
import HttpStatus from "../../httpStatus";

export async function light(req: ApiRequest, res: Express.Response, next: Express.NextFunction) {
    try {
        const HM = await HighMobility.create()

        const getLightResponse = await HM.getLightConditions()
        io.emit('light', getLightResponse)
        res.sendStatus(HttpStatus.OK)
    } catch(err) {
        console.log(err)
        next(err)
    }
}