import * as passport from 'passport'
import * as LocalStrategy from 'passport-local'
import * as bcrypt from 'bcrypt'
import * as Sequelize from 'sequelize'
import {Strategy as JwtStrategy, ExtractJwt} from 'passport-jwt'
import * as jwt from 'jsonwebtoken'
import {Response, NextFunction} from 'express'
import {compact, flatten} from 'lodash'

import {sync} from '../config'
import models from '../lib/models'
import {AuthorizableInstance, AuthorizableMemberAttributes} from "../lib/models/users"
import HttpStatus from "./httpStatus"
import {ApiRequest} from './metadata'
import config from "../config";
import DB from "./aws/dynamoDB";

const jwtOptions = {
    jwtFromRequest: ExtractJwt.fromAuthHeaderWithScheme('JWT'),
    secretOrKey: sync().jwtSecret
}

const modelForRole = {
    // TODO replace with mapping from role to user types entities
    user: models.User,
}

const UsersTableName = 'qtum-users'
const UsersAuthTableName = 'qtum-users-auth'

export type UserRole = keyof typeof modelForRole

interface User {
    id: string
    email?: string
    role: UserRole
}

export interface ApiUser extends User {}

declare global {
    namespace Express {
        interface Request {
            user?: any;
        }
    }
}

passport.serializeUser((user: ApiUser, done) => {
    let anyUser = user as any
    done(null, `${anyUser.role}/${anyUser.id}`)
})

passport.deserializeUser((serialized: string, done: (err?: Error, user?: ApiUser) => void) => {
    let [role, id] = serialized.split('/')
    modelForRole[role].find({where: {id}})
        .then(user => done(undefined, user))
        .catch(err => done(err))
})

interface RejectionInfo {
    message: string,
    statusCode?: HttpStatus
}

function localStrategy(role: UserRole) {
    const verify = async function (email, password, done: (err?: Error, user?: ApiUser, info?: RejectionInfo) => void) {
        try {
            const model = modelForRole[role]
            if (!model) {
                return done(new Error(`Unknown role '${role}', can't authorize.`))
            }

            const {aws: awsConfig} = await config()
            const db = new DB(awsConfig)

            const user: any = await  db.getItem(UsersTableName, {email: email.toLowerCase()})
            if (!user) {
                return done(undefined, undefined, {message: 'Unknown user'})
            }

            const passwordValid = await new Promise((resolve, reject) => {
                bcrypt.compare(password, user.password, (err, res) => {
                    if (err) {
                        return reject(err)
                    }
                    resolve(res)
                })
            })

            if (!passwordValid) {
                return done(undefined, undefined, {message: 'Invalid password'})
            }

            return done(undefined, {
                id: user.id,
                email: user.email,
                role: role
            })
        } catch (err) {
            done(err)
        }
    }

    return new LocalStrategy.Strategy({usernameField: 'email'}, verify)
}

function jwtStrategy(role: UserRole, audience?: string) {
    return new JwtStrategy({audience, ...jwtOptions}, async function (jwtPayload: { id: any, role: UserRole }, done: (err?: Error, user?: ApiUser) => void) {
        try {
            const model = modelForRole[role]
            if (!model) {
                return done(new Error(`Unknown role '${role}', can't authorize.`))
            }

            const {aws: awsConfig} = await config()
            const db = new DB(awsConfig)

            const userAuth: any = await  db.getItem(UsersAuthTableName, {id: jwtPayload.id})

            if (!userAuth) {
                return done()
            }

            const user: any = await db.getItem(UsersTableName, {email: userAuth.email})

            if (!user) {
                return done()
            }

            return done(undefined, {
                id: user.id!,
                email: user.email,
                role: jwtPayload.role
            })
        } catch (err) {
            done(err)
        }
    })
}

for (const role of Object.keys(modelForRole)) {
    passport.use(`local`, localStrategy(role as UserRole))
    passport.use(`jwt`, jwtStrategy(role as UserRole))
}

const authenticationCallback = function (req: ApiRequest, res: Response, next: NextFunction) {
    return (err?: Error, user?: ApiUser, info?: RejectionInfo | RejectionInfo[], statusCode?: HttpStatus | HttpStatus[]) => {
        if (err) {
            return next(err)
        }

        if (!user) {
            const infos = flatten(compact([info]))
            const statusCodes = compact([
                ...flatten(compact([statusCode])),
                ...infos.map(i => i.statusCode)
            ])

            req.logger.warn(...compact(infos.map(i => i.message)))
            return res.sendStatus((statusCodes.length && statusCodes[0]) || HttpStatus.unauthorized)
        }

        req.login(user, err => next(err || undefined))
    }
}

export const jwtAuth = () => (req: ApiRequest, res: Response, next: NextFunction) => {
    passport.authenticate('jwt', {session: false}, authenticationCallback(req, res, next))(req, res, next)
}

export function localAuth(req: ApiRequest, res: Response, next: NextFunction) {
    passport.authenticate('local', authenticationCallback(req, res, next))(req, res, next)
}

export function prepareToken(object) {
    return 'JWT ' + jwt.sign(object, jwtOptions.secretOrKey)
}
