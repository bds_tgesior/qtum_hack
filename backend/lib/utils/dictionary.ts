export interface ReadonlyDictionary<T> {
    readonly [index: string]: T;
}

export interface Dictionary<T> extends ReadonlyDictionary<T> {
    [index: string]: T;
}
