/* tslint:disable:no-unused-variable */
import {Router, RequestHandler} from 'express'
import * as multer from 'multer'

import {localAuth, jwtAuth} from './auth'
import {ApiRequest} from "./metadata"

import * as Auth from './app/authCtrl'
import {register as Register} from './register'

import * as CarCtrl from './app/car/carCtrl'
import * as IgnitionCtrl from './app/car/ignitionCtrl'
import * as DiagnosticsCtrl from './app/car/diagnosticsCtrl'
import * as MaintenanceCtrl from './app/car/maintenanceCtrl'
import * as ChargingCtrl from './app/car/chargingCtrl'
import * as ClimateCtrl from './app/car/climateCtrl'
import * as LocationCtrl from './app/car/locationCtrl'
import * as TrunkAccessCtrl from './app/car/trunkAccessCtrl'
import * as TheftAlarmCtrl from './app/car/theftAlarmCtrl'
import * as ParkingBrakeCtrl from './app/car/parkingBrakeCtrl'
import * as LightConditionsCtrl from './app/car/lightConditions'
import * as LightCtrl from './app/car/light'
import * as InsuranceCtrl from './app/insurance/insuranceCtrl'
import HighMobility from "../lib/highMobility/api";
import {io} from "../index";

const xlsxUpload = multer({
    fileFilter: (req: ApiRequest, file, callback) => {
        if (file && file.mimetype.toLowerCase() ===
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
            callback(null, true)
        } else {
            req.logger.warn(`Rejected upload of file with mime '${file && file.mimetype}'`)
            callback(null, false)
        }
    }
})

const apiPath = postfix => `/v1/${postfix}`
const webPath = postfix => `/${postfix}`

export default function(authorized: RequestHandler = jwtAuth()) {
    const router = Router()

    router.post(apiPath('register'), Register)
    router.post(apiPath('user/auth'), localAuth, Auth.login)

    router.post(apiPath('car/register'), authorized, CarCtrl.register)
    router.get(apiPath('car/:userId'), authorized, CarCtrl.get)

    router.post(apiPath('car/start'), authorized, IgnitionCtrl.start)
    router.post(apiPath('car/stop'), authorized, IgnitionCtrl.stop)
    router.post(apiPath('car/engine'), authorized, IgnitionCtrl.engine)

    router.post(apiPath('car/diagnostics'), authorized, DiagnosticsCtrl.diagnostics)

    router.post(apiPath('car/maintenance'), authorized, MaintenanceCtrl.maintenance)

    router.post(apiPath('car/charging'), authorized, ChargingCtrl.charging)

    router.post(apiPath('car/climate'), authorized, ClimateCtrl.climate)

    router.post(apiPath('car/location'), authorized, LocationCtrl.location)

    router.post(apiPath('car/trunk-access'), authorized, TrunkAccessCtrl.trunkAccess)

    router.post(apiPath('car/theft-alarm'), authorized, TheftAlarmCtrl.theftAlarm)

    router.post(apiPath('car/parking-brake'), authorized, ParkingBrakeCtrl.parkingBrake)

    router.post(apiPath('car/light-conditions'), authorized, LightConditionsCtrl.lightConditions)

    router.post(apiPath('car/light'), authorized, LightCtrl.light)

    router.post(apiPath('insurance'), authorized, InsuranceCtrl.start)

    // TODO fill as needed
    // router.get(apiPath('whatever'), authorized, Whatever.list)

    return router
}

export async function updateCarData() {
    const HMKit = await HighMobility.create()

    // const diagnostics = await HMKit.getDiagnostics()
    // io.emit('diagnostics', diagnostics)

    // const maintenance = await HMKit.getMaintenance()
    // io.emit('maintenance', maintenance)

    const charging = await HMKit.getCharging()
    io.emit('charging', charging)

    const climate = await HMKit.getClimate()
    io.emit('climate', climate)

    const location = await HMKit.getLocation()
    io.emit('location', location)

    const trunkAccess = await HMKit.getTrunkAccess()
    io.emit('trunkAccess', trunkAccess)

    const theftAlarm = await HMKit.getTheftAlarm()
    io.emit('theftAlarm', theftAlarm)

    const parkingBrake = await HMKit.getParkingBrake()
    io.emit('parkingBrake', parkingBrake)

    const lightConditions = await HMKit.getLightConditions()
    io.emit('lightConditions', lightConditions)

    const light = await HMKit.getLight()
    io.emit('light', light)
}