import * as React from 'react'
import {RegisterForm} from "../components/RegisterForm";
import {Redirect} from 'react-router';
import PageTitle from "../../layout/not_authenticated/components/PageTitle";

const qtumLogo = require('./../../../images/qtum_logo.png')

interface IRegisterPageState {
    error?: any,
    toDashboard: boolean
}

interface IRegisterPageDispatchProps {
    register?: (username: string, password: string) => void
}

class RegisterPage extends React.Component<IRegisterPageDispatchProps, IRegisterPageState> {

    constructor(props) {
        super(props)

        this.state = {
            toDashboard: false
        }
    }

    redirectToDashboard() {
        this.setState({
            toDashboard: true
        })
    }

    render() {

        if (this.state.toDashboard) {
            return <Redirect to="/"/>
        }

        return <React.Fragment>
            <div className="row justify-content-md-center">
                <div className="col-md-12">
                    <PageTitle title={"User register"} subTitle={"Register account with qtum blockchain service."}
                               img={qtumLogo}/>
                    <RegisterForm toDashboard={this.redirectToDashboard.bind(this)}/>
                </div>
            </div>
        </React.Fragment>
    }

}
export default RegisterPage