import {ApiRequest} from "../../metadata";
import * as Express from "express";
import HighMobility from "../../../lib/highMobility/api";
import {io} from "../../../index";
import HttpStatus from "../../httpStatus";

export async function lightConditions(req: ApiRequest, res: Express.Response, next: Express.NextFunction) {
    try {
        const HM = await HighMobility.create()

        const getLightConditionsResponse = await HM.getLightConditions()
        io.emit('lightConditions', getLightConditionsResponse)
        res.sendStatus(HttpStatus.OK)
    } catch(err) {
        console.log(err)
        next(err)
    }
}