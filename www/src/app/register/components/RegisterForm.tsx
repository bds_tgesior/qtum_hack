import * as React from 'react'
import {register} from "../registerApi"
import 'react-block-ui/style.css'

class IRegisterFormState {
    firstName: string = ''
    lastName: string = ''
    email: string = ''
    password: string = ''
    passwordRepeat: string = ''
    error: string = ''
    submitted: boolean = false

}

class IRegisterFormProps {
    toDashboard: () => void
}

export class RegisterForm extends React.Component<IRegisterFormProps, IRegisterFormState> {

    constructor(props) {
        super(props)

        this.state = new IRegisterFormState()

        this._onSubmit = this._onSubmit.bind(this)
        this._onEmailChange = this._onEmailChange.bind(this)
        this._onPasswordChange = this._onPasswordChange.bind(this)
        this._onFirstNameChange = this._onFirstNameChange.bind(this)
        this._onLastNameChange = this._onLastNameChange.bind(this)
        this._onPasswordRepeatChange = this._onPasswordRepeatChange.bind(this)
    }

    _onFirstNameChange(event) {
        this.setState({firstName: event.target.value})
    }

    _onLastNameChange(event) {
        this.setState({lastName: event.target.value})
    }

    _onEmailChange(event) {
        this.setState({email: event.target.value})
    }

    _onPasswordChange(event) {
        this.setState({password: event.target.value})
    }

    _onPasswordRepeatChange(event) {
        this.setState({passwordRepeat: event.target.value})
    }

    async validate() {
        console.log(this.state)
        if (!this.state.password || !this.state.lastName || !this.state.firstName || !this.state.email) {
            this.setState({error: `Please fill all form inputs.`})
            return false
        }

        if (this.state.password !== this.state.passwordRepeat) {
            this.setState({error: `Passwords does not match.`})
            return false
        }

        return true
    }

    async _onSubmit(event) {
        event.preventDefault()

        if (await this.validate() === true) {
            try {
                this.setState({submitted: true})
                const registered: any = await register({
                    firstName: this.state.firstName,
                    lastName: this.state.lastName,
                    email: this.state.email,
                    password: this.state.password
                })

                if (registered.status === 200) {
                    this.props.toDashboard()
                }
                this.setState({submitted: false})
            } catch (err) {
                this.setState({error: `User with given email exists.`, submitted: false})
            }

        }

    }

    render() {

        let allInputsClass = 'form-control w-50'

        let error: string | undefined

        if (this.state.error) {
            allInputsClass += ' invalid'
            error = this.state.error
        }

        return (<div className="row justify-content-center">
            <div className=" col-md-10 bg-white form-container">
               <div>
                    <h5 className="text-center pt-4">Hello! Please provide information to create new user.</h5>
                    <form className="form" onSubmit={this._onSubmit}>
                        <label className="row justify-content-center pt-4" htmlFor="register_firstName">First
                            Name</label>
                        <div className="form-group row justify-content-center">
                            <input id="register_firstName" type="text" value={this.state.firstName}
                                   onChange={this._onFirstNameChange}
                                   className={allInputsClass}/>
                        </div>
                        <label className="row justify-content-center" htmlFor="register_lastName">Last Name</label>
                        <div className="form-group row justify-content-center">
                            <input id="register_lastName" type="text" value={this.state.lastName}
                                   onChange={this._onLastNameChange}
                                   className={allInputsClass}/>
                        </div>
                        <label className="row justify-content-center" htmlFor="register_email">Email</label>
                        <div className="form-group row justify-content-center">
                            <input id="register_email" type="text" value={this.state.email}
                                   onChange={this._onEmailChange}
                                   className={allInputsClass}/>
                        </div>
                        <label className="row justify-content-center" htmlFor="register_password">Password</label>
                        <div className="form-group row justify-content-center">
                            <input id="register_password" type="password" value={this.state.password}
                                   onChange={this._onPasswordChange}
                                   className={allInputsClass}/>
                        </div>
                        <label className="row justify-content-center" htmlFor="register_passwordRepeat">Repeat
                            password</label>
                        <div className="form-group row justify-content-center">
                            <input id="register_passwordRepeat" type="password" value={this.state.passwordRepeat}
                                   onChange={this._onPasswordRepeatChange}
                                   className={allInputsClass}/>
                        </div>
                        <div className="form-group row justify-content-center">
                            <p className="text-danger">{error}</p>
                        </div>
                        <div className="form-group row justify-content-center pt-3 pb-5">
                            <input className="btn btn-theme w-50" type="submit" value="Submit"/>
                        </div>
                    </form>
                </div>
                }</div>
        </div>)
    }

}