import * as React from 'react';
import HeroSection from "../components/HeroSection";
import HeadlineSection from "../components/HeadlineSection";

export default class HomePage extends React.Component {

    constructor(props) {
        super(props)
    }

    render() {
        return <React.Fragment>
            <div>
                <HeroSection/>
                <div className="mt-5">
                    <HeadlineSection primaryTitle={"Car insurance"} secondaryTitle={"Data analyst"}/>
                </div>
                <div className="mt-5 mb-5">
                    <HeadlineSection primaryTitle={"Live data tracking"} secondaryTitle={"Blockchain storage"}/>
                </div>
            </div>
        </React.Fragment>
    }

}