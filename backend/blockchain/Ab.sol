pragma solidity ^0.4.11;

contract Ab {
    uint256 a;

    constructor(uint256 _a) public {
        a = _a;
    }

    function getA() public returns(uint256) {
        return a;
    }
}