#!/usr/bin/env bash

PROJECT_NAME="TODO"
PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." && pwd )"

if [ -z "${env}" ]; then
    echo "env is empty"
    exit 1
fi

if [ -z "${aws_profile}" ]; then
  aws="aws"
else
  aws="aws --profile $aws_profile"
fi

set -eu

pushd ${PROJECT_DIR}

npm run --prefix build generate-aws-template

stack_name="${PROJECT_NAME}-app-${env}"
stack_exists=$(${aws} cloudformation list-stacks --query "StackSummaries[?StackName == '${stack_name}' && StackStatus != 'DELETE_COMPLETE']" --region eu-central-1)

deploy_params="\
ParameterKey=DeployEnv,ParameterValue=${env} \
ParameterKey=AppImage,ParameterValue=${app_image} \
ParameterKey=BackendDbPassword,ParameterValue=${backend_db_password}"

if [[ ${stack_exists} =~ "[]" ]]; then
    echo "Create new app stack: ${stack_name}"
    set +e
    result=$(${aws} cloudformation create-stack --stack-name ${stack_name} \
      --region eu-central-1 \
      --disable-rollback \
      --capabilities CAPABILITY_IAM \
      --template-body "$(cat ./build/aws.template)" \
      --parameters ${deploy_params} 2>&1)

    exit_code=$?
    set -e
    if [[ ${exit_code} != 0 ]]; then
        echo "Error occurred"
        echo ${result}
        exit ${exit_code}
    fi

    ${aws} cloudformation wait stack-create-complete --stack-name ${stack_name}
    echo "Stack ${stack_name} create complete."
else
    echo "Update existing app stack: ${stack_name}"
    set +e
    result=$(${aws} cloudformation update-stack --stack-name ${stack_name} \
      --region eu-central-1 \
      --capabilities CAPABILITY_IAM \
      --template-body "$(cat ./build/aws.template)" \
      --parameters ${deploy_params} 2>&1)

    exit_code=$?
    set -e
    if [[ ${exit_code} != 0 ]]; then
        if [[ ! ${result} =~ "No updates are to be performed" ]]; then
            echo "Error occurred"
            echo ${result}
            exit ${exit_code}
        else
            echo "No updates to be performed, exiting."
            exit 0
        fi
    fi

    ${aws} cloudformation wait stack-update-complete --stack-name ${stack_name}
    echo "Stack ${stack_name} update complete."
fi

popd
