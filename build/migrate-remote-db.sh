#!/usr/bin/env bash
set -e

PROJECT_NAME="TODO"

env=$(echo ${env} | awk '{print tolower($0)}')
echo "Using ${PROJECT_NAME} environment ${env}"

vpc=$(aws ec2 describe-vpcs --query "Vpcs[?Tags[?contains(Value, '${env}')]].VpcId | [0]" --output text)
echo "Using vpc ${vpc}"

bastion_host_ip=$(aws ec2 describe-instances --query "Reservations[?Instances[?VpcId=='${vpc}' && Tags[?Value=='BastionHost']]] | [0].Instances[0].PublicIpAddress"  --output text)
echo "Using host ${bastion_host_ip}"

db_instance=$(aws cloudformation list-stack-resources --stack-name "${PROJECT_NAME}-app-${env}" --query 'StackResourceSummaries[?LogicalResourceId==`BackendDBInstance`].PhysicalResourceId | [0]' --output text)
db_address=$(aws rds describe-db-instances --db-instance-identifier ${db_instance} --query 'DBInstances[0].Endpoint.Address' --output text)

local_port=5433
if [[ ${env} == "production" ]]; then
    local_port=5434
fi

echo "Open tunnel through ${bastion_host_ip} to ${db_address}:5432 listening locally on ${local_port}"
#http://superuser.com/questions/96489/ssh-tunnel-via-multiple-hops
ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -L ${local_port}:${db_address}:5432 -N ec2-user@${bastion_host_ip} &

sleep 5 # give the tunnel a moment to initialize
cd .. && build/node_modules/.bin/sequelize db:migrate --options-path sequelize.json --url postgresql://${PROJECT_NAME}:${backend_db_password}@localhost:${local_port}/${PROJECT_NAME}
return=$?

kill %1 # kills ssh connection

exit ${return}