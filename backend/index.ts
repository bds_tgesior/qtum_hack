import * as express from 'express'
import * as bodyParser from 'body-parser'
import * as passport from 'passport'
import * as i18n from 'i18n'
import {join} from 'path'
import {curry} from 'lodash'
const socketIO = require('socket.io')
import * as cron from 'node-cron'

import {logger, bindLogger, expressError as logExpressError} from './api/logger'
import {enhanceRequest} from './api/metadata'
import api from './api'
import healthcheck from './api/healthcheck'
import HttpStatus from './api/httpStatus'
import HttpError from './api/httpError'
import {updateCarData} from "./api/router";
import {startInterval} from "./api/app/car/carCtrl";
const cors = require('cors')

const PORT = process.env.PORT || 3001
const NODE_ENV = process.env.NODE_ENV || 'development'

const app = express()

app.use(bodyParser.json())
app.use(cors())

app.use(passport.initialize())
i18n.configure({
    directory: join(__dirname, '..', 'assets/translations'),
    updateFiles: false,
    objectNotation: true
})
app.use(i18n.init)

app.get('/health', healthcheck)

const apiPrefix = '/api'
app.use(apiPrefix + '/:any', express.static('../www/static'))
app.use(apiPrefix, enhanceRequest)
app.use(apiPrefix, bindLogger)
app.use(apiPrefix, api)

if (NODE_ENV === 'development') {
    app.use('/node_modules', express.static('../www/node_modules'))
}

app.use(express.static('../www/bin'))
app.use(apiPrefix, curry(logExpressError)(new HttpError(HttpStatus.notFound, 'Unknown API URL')))
app.use(/^\/[^.]+$/, express.static('../www/bin'))

app.use(logExpressError)

app.set('trust proxy', true)

const errorFallback = err => {
    logger.error(err)
    process.exit(1)
}

process.on('uncaughtException', errorFallback)
process.on('unhandledRejection', errorFallback)

const server = app.listen(PORT, () => logger.info(`Server listening on *:${PORT}`))

export const io = socketIO(server)
io.on('connection', function(socket){
    console.log('a user connected');
    socket.on('disconnect', () => {
        console.log('user disconnected')
    })
});

cron.schedule("*/10 * * * * *", async () => {
    await updateCarData()
});

startInterval()

module.exports = app