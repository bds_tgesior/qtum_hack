import * as Sequelize from 'sequelize'
import {token, v4} from 'gen-uid'

import {AuthorizableInstance, AuthorizableMemberAttributes, createHashFor, createSaltedPassword} from "./users"
import {HasTimestamps} from "./index"

export class XAttributes implements AuthorizableMemberAttributes {
    id: string
    name: string
    email: string
    password: string
    salt: string
    passwordResetToken?: string
    passwordResetSentAt?: Date

    constructor(attrs: XAttributes) {
        this.id = attrs.id
        this.name = attrs.name
        this.email = attrs.email
    }
}

export interface XInstance extends Sequelize.Instance<XAttributes>, XAttributes, AuthorizableInstance, HasTimestamps {
    requestPasswordResetToken(): Promise<string>

    resetPassword(string): Promise<void>
}

export interface XModel extends Sequelize.Model<XInstance, XAttributes> {
}

export default function X(sequelize: Sequelize.Sequelize): XModel {
    const fields = {
        id: {
            type: Sequelize.STRING(36),
            primaryKey: true
        },
        name: {
            type: Sequelize.STRING,
            allowNull: false
        },
        email: {
            type: Sequelize.STRING,
            allowNull: false,
            unique: true,
            validate: {
                isEmail: true
            }
        },
        password: {
            type: Sequelize.STRING
        },
        salt: {
            type: Sequelize.STRING
        },
        passwordResetToken: {
            type: Sequelize.STRING,
            field: 'password_reset_token'
        },
        passwordResetSentAt: {
            type: Sequelize.DATE,
            field: 'password_reset_sent_at'
        }
    }

    const config = {
        timestamps: true,
        underscored: true,
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }

    const X = sequelize.define<XInstance, XAttributes>('x', fields, config) as XModel

    X.beforeValidate(model => {
        if (!model.id) {
            model.id = v4()
        }
    })

    X.beforeCreate(user => {
        user.password = token() + token() // generate random password
        return createSaltedPassword(user)
    })

    X.prototype.requestPasswordResetToken = async function () {
        const plainToken = token() + token()
        const passwordResetToken = await createHashFor(plainToken, this.salt)

        await this.update({
            passwordResetToken,
            passwordResetSentAt: new Date()
        })

        return plainToken
    }

    X.prototype.resetPassword = async function (newPassword: string) {
        await this.update({
            password: await createHashFor(newPassword, this.salt),
            passwordResetToken: null,
            passwordResetSentAt: null
        })
    }

    return X
}