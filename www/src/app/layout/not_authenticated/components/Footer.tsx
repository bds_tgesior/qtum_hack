import * as React from 'react';

const qtumLogo = require('../../../../images/qtum_brand_logo.svg')

interface IFooterNavUl {
    styles: string,
    title: string
}

const FooterNavUl: React.SFC<IFooterNavUl> = (props) => {

    return (
        <div className="col-6 col-md">
            <h5>{props.title}</h5>
            <ul className={props.styles}>
                <li><a className="text-muted" href="#">Cool stuff</a></li>
                <li><a className="text-muted" href="#">Random feature</a></li>
                <li><a className="text-muted" href="#">Team feature</a></li>
                <li><a className="text-muted" href="#">Stuff for developers</a></li>
                <li><a className="text-muted" href="#">Another one</a></li>
                <li><a className="text-muted" href="#">Last time</a></li>
            </ul>
        </div>
    )

}

interface IFooterLogo {
    img: any
}

const FooterLogo: React.SFC<IFooterLogo> = (props) => {

    return (
        <div className="col-12 col-md">
            <img src={props.img} width="50px"/>
            <small className="d-block mb-3 pl-2 text-muted">© 2017-2018</small>
        </div>
    )

}

export default function Footer() {

    return <React.Fragment>
        <footer className="container py-5">
            <div className="row">
                <FooterLogo img={qtumLogo}/>
                <FooterNavUl styles="list-unstyled text-small" title="Resources"/>
                <FooterNavUl styles="list-unstyled text-small" title="Resources"/>
                <FooterNavUl styles="list-unstyled text-small" title="Resources"/>
                <FooterNavUl styles="list-unstyled text-small" title="Resources"/>
            </div>
        </footer>
    </React.Fragment>

}