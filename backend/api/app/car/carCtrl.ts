import * as Express from 'express'
import {ApiRequest} from "../../metadata";
import HttpError from "../../httpError";
import HttpStatus from "../../httpStatus";
import config from "../../../config";
import DB from "../../aws/dynamoDB";
import * as uniqid from 'uniqid';
import QtumContract from "../../../blockchain/api";
import {io} from "../../../index";

const CarTableName = 'qtum-cars'

let speed = 0, mileage = 0, tires = 0, temperature = 0, kilometersToNextService = 0

export async function register(req: ApiRequest, res: Express.Response, next: Express.NextFunction) {

    const {vin, carCompany, carModel, mileage, productionYear} = req.body.car

    if (!vin || !carCompany || !carModel || !mileage || !productionYear) {
        return new next(new HttpError(HttpStatus.noContent, `Missing car registration params.`))
    }

    const userId = req.user!.id

    if (!userId) {
        return new next(new HttpError(HttpStatus.noContent, `Missing user id.`))
    }

    try {
        const {aws: awsConfig, blockchain: blockchain} = await config()
        const db = new DB(awsConfig)

        const qtum = QtumContract.create('CarLiabilityInsurance.sol').getInstance()

        const tx = await qtum.send("registerCar", [productionYear, carCompany, carModel, vin, mileage], {
            senderAddress: blockchain.walletAddress
        })

        console.log(tx)

        const currentCarsItem = await db.getItem(CarTableName, {userId} as any)
        const currentCars: Array<any> = currentCarsItem ? currentCarsItem.cars as any : []
        const newCars = [
            ...currentCars, {
                carId: uniqid(),
                carCompany,
                carModel,
                productionYear,
                vin,
                mileage
            }]

        await db.putItem(CarTableName, {
            userId,
            cars: newCars
        })


        res.send({status: HttpStatus.OK})

    } catch (err) {
        console.log(err)
        next(err)
    }
}

const baseTemperature = 30
const maxSpeed = 140
const baseKilometersToNextService = 30000

export function startInterval() {
    mileage = 0
    speed = 0
    tires = 100
    temperature = baseTemperature
    kilometersToNextService = baseKilometersToNextService
    setInterval(() => {
        const random = Math.random() * 20
        const amount = Math.random() * 20
        if (random > 4) {
            speed += amount
        }
        if (random < 2) {
            speed -= 0.5 * amount
        }
        if (speed < 0) {
            speed = 0
        }
        mileage += (speed * 0.277777778)

        tires -= (Math.sqrt(speed / 100)) / 100
        if (tires < 0) {
            tires = 0
        }
        temperature = baseTemperature + 70 * (speed / maxSpeed)
        const decreaseSpeedAmount = Math.random() % 10 + 10
        if (temperature > 95) {
            speed -= decreaseSpeedAmount
        }

        kilometersToNextService = baseKilometersToNextService - mileage
        if (kilometersToNextService < 0) {
            kilometersToNextService = 0
        }
        io.emit('maintenance', {kilometersToNextService})
        const resp = {speed, mileage, tires, temperature}
        io.emit('diagnostics', resp)
    }, 1000)
}

export async function get(req: ApiRequest, res: Express.Response, next: Express.NextFunction) {
    const userId = req.params.userId

    if (!userId) {
        return new next(new HttpError(HttpStatus.noContent, `Missing user id.`))
    }

    try {
        const {aws: awsConfig} = await config()
        const db = new DB(awsConfig)

        const cars = await db.getItem(CarTableName, {userId} as any)

        res.send(cars || {userId: userId, cars: null})

    } catch (err) {
        console.log(err)
        next(err)
    }
}