import {ApiRequest} from "../../metadata";
import * as Express from "express";
import HighMobility from "../../../lib/highMobility/api";
import {io} from "../../../index";
import HttpStatus from "../../httpStatus";

export async function maintenance(req: ApiRequest, res: Express.Response, next: Express.NextFunction) {
    try {
        const HM = await HighMobility.create()

        const getMaintenanceResponse = await HM.getMaintenance()
        io.emit('maintenance', getMaintenanceResponse)
        res.sendStatus(HttpStatus.OK)
    } catch(err) {
        console.log(err)
        next(err)
    }
}