import * as React from 'react'
import PageTitle from '../layout/not_authenticated/components/PageTitle'
import {CarTile} from './carTile/CarTile'
import {apiUserStore} from '../apiStore'
import {Car, getCars} from '../car/carApi'
import {withRouter} from 'react-router'
import './DashboardMainPage.css'
import {LiveData} from './liveDataComponent/LiveDataComponent'

interface IDashboardMainPageProps {
    history: any
}

interface IDashboardMainPageState {
    cars: Array<Car>
    isLiveBoardActive: boolean
    activeCar: Car
}

const qtumLogo = require('./../../images/qtum_brand_logo.svg')

class DashboardMainPage extends React.Component<IDashboardMainPageProps, IDashboardMainPageState> {
    constructor(props) {
        super(props)
        this.state = {cars: [], isLiveBoardActive: false, activeCar: null}
    }

    async componentDidMount() {
        const cars = await getCars(apiUserStore!.get()!.id)
        this.setState({cars})
    }

    onLiveDataClicked = (carId: string) => {
        this.setState({isLiveBoardActive: true, activeCar: this.state.cars.find(car => car.carId === carId)})
    }

    onBuyInsurance(carId) {
        this.props.history.push({
            pathname: '/insurance',
            carId: carId
        })
    }

    onCloseButtonClicked = () => {
        this.setState({isLiveBoardActive: false, activeCar: null})
    }

    render() {
        const {cars, isLiveBoardActive, activeCar} = this.state
        const carTilesColSize = isLiveBoardActive ? 6 : 12

        let insurance:any = {}

        if (activeCar && activeCar.insurance) {
            insurance = activeCar.insurance
        }

        return (
            <div className="row justify-content-center">
                <div className="col-md-12">
                    <PageTitle title={"Cars Dashboard"} subTitle={"See details or buy insurance for your cars."}
                               img={qtumLogo}/>
                    <div className="row justify-content-center">

                        <div className={`col-md-${carTilesColSize} bg-white form-container`}>
                            <main role="main" className="col-md-10 offset-md-1">
                                {cars ? activeCar ? <CarTile key={activeCar.carId} carId={activeCar.carId}
                                                             carCompany={activeCar.carCompany} vin={activeCar.vin}
                                                             productionYear={activeCar.productionYear}
                                                             carModel={activeCar.carModel}
                                                             insurance={activeCar.insurance}
                                                             onLiveDataClicked={this.onLiveDataClicked}
                                                             onBuyInsurance={this.onBuyInsurance.bind(this)}
                                                             isForActiveCar={true}/> : <div
                                    className="card-deck">
                                    {cars.map(car => <CarTile key={car.carId} carId={car.carId}
                                                              carCompany={car.carCompany}
                                                              vin={car.vin}
                                                              productionYear={car.productionYear}
                                                              carModel={car.carModel}
                                                              insurance={car.insurance}
                                                              onLiveDataClicked={this.onLiveDataClicked}
                                                              onBuyInsurance={this.onBuyInsurance.bind(this)}
                                                              isForActiveCar={false}/>)}
                                </div> : <div>
                                    <h5 className="text-center text-padding">It seems you have no cars
                                        registered...</h5>
                                    <h1 className={"text-center pt-1"}>Please, register your car first</h1>
                                    <div className="text-center button-padding">
                                        <button onClick={() => this.props.history.push('/car-register')}
                                                className="btn btn-theme">Car register
                                        </button>
                                    </div>
                                </div>
                                }
                            </main>
                        </div>
                        {activeCar &&
                        <LiveData carId={activeCar.carId} onCloseButtonClicked={this.onCloseButtonClicked}
                                  isLiveBoardActive={isLiveBoardActive} cap={insurance.cap}
                                  coverage={insurance.coverage} rate={insurance.rate}/>}
                    </div>
                </div>
            </div>
        )
    }

}

export default withRouter(DashboardMainPage)