import * as React from 'react';
import * as Modal from 'react-modal';
import {start} from "../insuranceApi";
import {withRouter} from "react-router";


interface IInsuranceModalProps {
    history: any
    isOpen: boolean,
    hideModal: () => void
    carId: string
    coverage: any
    rate: any
    cap: any
}

interface IInsuranceModalState {

}

const customStyles = {
    content: {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)',
        width: '50%',
        height: 'auto'
    }
};


class InsuranceModal extends React.Component<IInsuranceModalProps, IInsuranceModalState> {

    constructor(props) {
        super(props)
    }

    async onStartInsurance() {
        const result:any = await start(this.props.carId, this.props.coverage, this.props.cap, this.props.rate)
        if (result.status === 200) {
            this.props.history.push("/dashboard")
        }
    }


    render() {

        return (
            <React.Fragment>
                <Modal
                    isOpen={this.props.isOpen}
                    onRequestClose={this.props.hideModal}
                    style={customStyles}
                >
                    <div className="container-fluid justify-content-center">

                        <div className="row justify-content-center">
                            <h4 className="font-weight-bold pt-5">INSURANCE PROPOSAL</h4>
                        </div>
                        <div className="row justify-content-center pt-4">
                            <div className="my-3 p-3 bg-white rounded box-shadow">
                                <h6 className="border-bottom border-gray pb-2 mb-0">insurance & car informations</h6>
                                <div className="media text-muted pt-3">
                                        <p className="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                                            <strong className="d-block text-gray-dark">@rate per kilometer</strong>
                                           This rate is based on your live data, collected by our sensors and stored on QTUM blockchain.
                                        </p>
                                    <span className="pull-right light-red">{this.props.rate}</span>
                                </div>
                                <div className="media text-muted pt-3">
                                        <p className="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                                            <strong className="d-block text-gray-dark">@insurance cap</strong>
                                            Amount of money to spend for this insurance.
                                        </p>
                                    <span className="pull-right light-red">{this.props.cap}</span>
                                </div>
                                <div className="media text-muted pt-3">
                                        <p className="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                                            <strong className="d-block text-gray-dark">@coverage</strong>
                                            Insurance package choosen for this car.
                                        </p>
                                    <span className="pull-right light-red">{this.props.coverage}</span>
                                </div>
                                <small className="d-block text-right mt-3">
                                    <a href="#">secured by QTUM blockchain</a>
                                </small>
                            </div>
                            <div className="col-md-10">
                                <div className="form-group row justify-content-center pt-5 pb-5">
                                    <button onClick={() => this.onStartInsurance()}
                                            className="btn btn-theme text-center">Accept & Start insurance
                                    </button>
                                </div>

                            </div>
                        </div>
                    </div>
                </Modal>
            </React.Fragment>
        )
    }
}

export default withRouter(InsuranceModal)