import * as React from 'react';
import {Link} from 'react-router-dom';
import {apiTokenStore, apiUserStore} from "../../../apiStore";
import {
    withRouter
} from 'react-router-dom';

const qtumLogo = require('../../../../images/qtum_brand_logo.svg')

interface INavLiProps {
    name: string,
    style: string,
    to: string
}

const NavLi: React.SFC<INavLiProps> = (props) => {

    return (
        <li className={props.style}>
            <Link className="nav-link text-black-50 text-uppercase" to={props.to}>{props.name}</Link>
        </li>
    )
}

interface INavList {
    logout: () => void
}

const NavList: React.SFC<INavList> = (props) => {

    return (
        <ul className="navbar-nav ml-auto text-size--small">
            <NavLi name='Dashboard' style='nav-item pr-5' to="/dashboard"/>
            <NavLi name='Car Register' style='nav-item pr-5' to="/car-register"/>
            <button onClick={props.logout}
                    className="btn btn-theme">logOut
            </button>
        </ul>
    )
}


interface INavLogoProps {
    img: any,
    userEmail: string
}

const NavLogo: React.SFC<INavLogoProps> = (props) => {
    return (
        <Link className="navbar-brand light-blue" to={"/dashboard"}><img width="55px" src={props.img}/> {props.userEmail}</Link>
    )
}

interface INavProps {
    history: any
}


interface INavState {
    userEmail: string
}

class Nav extends React.Component<INavProps, INavState> {

    constructor(props) {
        super(props)
        this.state = {
            userEmail : apiUserStore.get()!.email
        }
    }

    logout() {
        apiTokenStore.clear()
        apiUserStore.clear()
        this.props.history.push('/')
    }

    public render() {
        return (
            <nav className="navbar navbar-expand-lg bg-white bg-dark">
                <NavLogo img={qtumLogo} userEmail={this.state.userEmail}/>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                        aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"/>
                </button>
                <div className="collapse navbar-collapse" id="navbarNav">
                    <div className="top-menu-bg"/>
                    <NavList logout={this.logout.bind(this)}/>
                </div>
            </nav>
        )
    }
}

export default withRouter(Nav);