export default class HttpError extends Error {

    statusCode: number
    jsonResponse?: any

    constructor(statusCode: number, message: string | Error) {
        super(message.toString());
        this.statusCode = statusCode;
    }

    withJsonResponse(json: any): HttpError {
        this.jsonResponse = json
        return this
    }
}