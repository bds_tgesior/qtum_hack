# MONDAYS - Car Liability Insurance on QTUM #

*This project will be submitted to the [QTUM Global Hackathon](https://hackathon.qtum.org/en) ending on Sunday the 16th of September 2018.*   

Mondays provides a dynamic, on-demand car liability insurance, ensuring the truthfulness of your data on the QTUM blockchain.

The project uses the following tools and frameworks:
* Node.js (latest LTS version) with Express server
    * logging infrastructure
    * base user auth route templates
    * Sequelize with basic model template
    * snippets for Excel import/export and file upload
* Mocha/Chai backend unit tests
* AWS configuration & orchestration via AWS CloudFormation
    * deployment to EC2 Container Service (Docker containers)
    * S3 file upload
    * SES email sending
* React frontend with Redux, Webpack
    * separate "main" and "forgot password" websites
* TeamCity-ready (npm and shell scripts)
* QTUM
   * [solar](https://github.com/qtumproject/solar) - for deployment of smart contract
   * qmix
  


## To test it locally

* `nvm use`
* `npm install`
* `npm run start` (to start backend at [localhost:3000](http://localhost:3000))

To use AWS stuff locally, create `.env` file in root with the following content:

```
AWS_ACCESS_KEY_ID=[AWS API key]
AWS_SECRET_ACCESS_KEY=[AWS Secret for dev API user]
```


## Architecture

The project consists of the following components:
* Backend 
    * High-mobility API connection
    * 
* Frontend

The backend architecture is described by a CloudFormation template, so it follows the IaC concept (Infrastructure as Code). 
* Frontend -  

### Bastion host

It's an instance that should be use when connecting/accessing one of AWS stack resource, so it should be the only gateway to the outside world except Load Balancer that takes HTTP inbound traffic. Bastion configured in public subnet with specific security group `BastionSecurityGroup` that by default accepts SSH traffic. Bastion is also used for connecting to database, please see script: `build/connect-db.sh`

### Database

Database is hosted using AWS RDS service, it's Postgres database. 

### Network configuration

All resources are inside one VPC, four subnets, two private subnets and two public subnets to support multi AZ. Load Balancer is configured in public subnets, ECS instances and Database configured in private subnet.   
