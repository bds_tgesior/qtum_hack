import * as React from 'react';
import {HeaderContainer} from "./HeaderContainer";
import Footer from "../components/Footer";
import {Redirect} from "react-router";

interface INotAuthenticatedContainerProps {
    isAuthenticated: () => boolean
}

interface INotAuthenticatedContainerState {

}

export default class NotAuthenticatedContainer extends React.Component<INotAuthenticatedContainerProps, INotAuthenticatedContainerState> {

    constructor(props) {
        super(props)
    }


    render() {

        if (this.props.isAuthenticated()) {
            return <Redirect to={'/dashboard'}/>
        }

        return (
            <React.Fragment>
                <HeaderContainer/>
                <div className="main container-fluid">
                    {this.props.children}
                </div>
                <Footer/>
            </React.Fragment>
        )
    }

}