import {DynamoDB} from 'aws-sdk'

export default class DB {
    private documentClient: DynamoDB.DocumentClient

    constructor(config: {region: string}) {
        this.documentClient = new DynamoDB.DocumentClient({
            region: config.region
        })
    }

    async putItem(tableName: string, item: DynamoDB.DocumentClient.PutItemInputAttributeMap): Promise<void> {
        const params = {
            TableName: tableName,
            Item: item
        };

        await this.documentClient.put(params).promise()
    }

    async getItem(tableName: string, key: DynamoDB.Key): Promise<DynamoDB.AttributeMap | undefined> {
        const params = {
            TableName: tableName,
            Key: key
        };

        const result = await this.documentClient.get(params).promise()
        return result.Item
    }

}
