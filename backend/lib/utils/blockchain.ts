export default interface Blockchain {
    walletAddress: string,
    privateKey: string
}