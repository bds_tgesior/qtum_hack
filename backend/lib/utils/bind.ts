export function bind<T extends Function>(func: T, that: object): T {
    return func.bind(that)
}