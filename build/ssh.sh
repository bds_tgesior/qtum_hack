#!/usr/bin/env bash -e

PROJECT_NAME="TODO"

if [ -z "${env}" ]; then
    echo "env is empty"
    exit 1
fi

if [ -z "${aws_profile}" ]; then
  aws="aws"
else
  aws="aws --profile $aws_profile"
fi

env=$(echo ${env} | awk '{print tolower($0)}')
echo "Using ${PROJECT_NAME} environment ${env}"

vpc=$(${aws} ec2 describe-vpcs --query "Vpcs[?Tags[?contains(Value, '${env}')]].VpcId | [0]" --output text)
echo "Using vpc ${vpc}"

bastion_host_ip=$(${aws} ec2 describe-instances --query "Reservations[?Instances[?VpcId=='${vpc}' && Tags[?Value=='BastionHost']]] | [0].Instances[0].PublicIpAddress"  --output text)
echo "Using host ${bastion_host_ip}"

db_instance=$(${aws} cloudformation list-stack-resources --stack-name "${PROJECT_NAME}-app-${env}" --query 'StackResourceSummaries[?LogicalResourceId==`BackendDBInstance`].PhysicalResourceId | [0]' --output text)
db_address=$(${aws} rds describe-db-instances --db-instance-identifier ${db_instance} --query 'DBInstances[0].Endpoint.Address' --output text)

ssh -i ~/.ssh/${PROJECT_NAME}/ssh-access.pem ec2-user@${bastion_host_ip}
