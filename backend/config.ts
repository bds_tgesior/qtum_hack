import * as _ from 'lodash'
import * as path from "path"
import * as fs from "fs"
import AwsConfig from './lib/aws/awsConfig'
import Blockchain from "./lib/utils/blockchain";

const env = process.env.NODE_ENV || 'development'

if (env === 'development' || env === 'test-local') {
    let envPath = '';
    while (!fs.existsSync(path.join(envPath, '.env')) && path.resolve(envPath) !== '/') {
        envPath = path.join('..', envPath)
    }

    console.log(`Using .env from '${path.resolve(envPath)}', if exists.`)
    require('dotenv').config({path: path.join(envPath, '.env')})
}

const getDatabaseUrl = () => {
    return `postgres://test@test`
}

const envSpecificConfig = {
    test: {},
    stage: {
        basePath: 'https://TODO-stage.org'
    },
    production: {
        basePath: 'https://TODO.org',
    }
}

const baseConfig = {
    env,
    basePath: 'http://localhost:3002',
    database: getDatabaseUrl(),
    jwtSecret: ['development', 'test', 'test-local'].includes(env) ? 'ge#$lkjSDkeljgdekljrde' : process.env.JWT_SECRET,
    aws: {
        s3Bucket: `TODO-uploads-${env}`,
        s3LinkExpirationSeconds: 3600 * 24,
        region: 'eu-central-1',
        credentials: {
            accessKeyId: process.env.AWS_ACCESS_KEY_ID,
            secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY
        }
    } as AwsConfig,
    ses: {
        from: 'noreply@TODO.org',
        accessKeyId: process.env.AWS_ACCESS_KEY_ID,
        secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
        region: process.env.SES_REGION || 'eu-west-1'
    },
    passwordResetValidHours: 24,
    highMobility: {
        accessToken: process.env.HIGH_MOBILITY_ACCESS_TOKEN,
        certificate: process.env.HIGH_MOBILITY_CERTIFICATE,
        certificateHash: process.env.HIGH_MOBILITY_CERTIFICATE_HASH
    },
    blockchain: {
        walletAddress: process.env.QTUM_ADDRESS,
        privateKey: process.env.QTUM_PRIVATE_KEY
    } as Blockchain
}

const resolvedConfig = _.merge(baseConfig, envSpecificConfig[env] || {})

export default function config() {
    return Promise.resolve(resolvedConfig)
}

export function sync() {
    // TODO replace with async above
    return resolvedConfig
}