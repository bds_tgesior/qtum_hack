import * as React from 'react';
import Footer from "../components/Footer";
import {HeaderContainer} from "./HeaderContainer";
import {Redirect} from "react-router";


interface IAuthenticatedContainerProps {
    isAuthenticated: () => boolean
}

interface IAuthenticatedContainerState {

}

export default class AuthenticatedContainer extends React.Component<IAuthenticatedContainerProps, IAuthenticatedContainerState> {


    constructor(props) {
        super(props)
    }

    public render() {

        if (!this.props.isAuthenticated()) {
            return <Redirect to={'/'}/>
        }

        return (
            <React.Fragment>
                <HeaderContainer/>
                <div className="main container-fluid">
                    {this.props.children}
                </div>
                <Footer/>

            </React.Fragment>
        )
    }


}