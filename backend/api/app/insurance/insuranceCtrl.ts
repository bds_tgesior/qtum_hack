import * as Express from 'express'
import {ApiRequest} from "../../metadata";
import QtumContract from "../../../blockchain/api";
import config from "../../../config";
import DB from "../../aws/dynamoDB";
import HttpStatus from "../../httpStatus";
import HttpError from "../../httpError";

const CarTableName = 'qtum-cars';

export async function start(req: ApiRequest, res: Express.Response, next: Express.NextFunction) {

    let {carId, coverage, cap, rate} = req.body

    const userId = req.user!.id

    if (!userId) {
        return new next(new HttpError(HttpStatus.noContent, `Missing user id.`))
    }

    try {
        const {aws: awsConfig, blockchain: blockchain} = await config()
        const qtum = QtumContract.create('CarLiabilityInsurance.sol').getInstance()

        // const tx = await qtum.send("startInsuranceUser", [carId, coverage, cap], {
        //     senderAddress: blockchain.walletAddress
        // })

        const db = new DB(awsConfig)

        const currentCarsItem = await db.getItem(CarTableName, {userId} as any)
        const currentCars: Array<any> = currentCarsItem ? currentCarsItem.cars as any : []

        const currentCar: any = currentCars.find((car) => car.carId === carId)
        const otherCars = currentCars.filter((car) => car.carId !== carId)
        currentCar.insurance = {
            coverage,
            cap,
            rate
        }

        await db.putItem(CarTableName, {
            userId,
            cars: [...otherCars, currentCar]
        })

        res.send({status: HttpStatus.OK})

    } catch (err) {
        console.log(err)
        next(err)
    }

}