export default interface AwsConfig {
    s3Bucket: string
    s3LinkExpirationSeconds: number
}
