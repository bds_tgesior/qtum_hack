
import {Request, Response} from 'express';

import {logger} from './logger';
// import models from '../models';

export default async (req: Request, res: Response, next) => {
    // const User = models.User;
    try {
        // const count = await User.count();
        logger.info(`Health OK.`);
        res.send('Hello');
    } catch (err) {
        next(err);
    }
}