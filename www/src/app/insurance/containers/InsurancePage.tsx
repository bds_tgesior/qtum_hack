import * as React from 'react';
import InsuranceModal from "../components/InsuranceModal";
import {Redirect, withRouter} from "react-router";

interface IInsurancePageProps {
    history: any
    location:any
}

interface IInsurancePageState {
    modalIsOpen: boolean
    carId: any
    coverage: any
    rate: any
    cap: any
}


class InsurancePage extends React.Component<IInsurancePageProps, IInsurancePageState> {

    constructor(props) {
        super(props)
        this.state = {
            modalIsOpen: false,
            carId: this.props.location.carId,
            coverage: '',
            rate: '',
            cap: ''

        }
        this.showModal.bind(this)
    }

    showModal(coverage, rate, cap) {
        this.setState({
            coverage: coverage,
            rate: rate,
            cap: cap,
            modalIsOpen: true
        })
    }

    onBack() {
        this.props.history.push("/dashboard")
    }

    hideModal() {
        this.setState({modalIsOpen: false})
    }

    render() {

        if (!this.state.carId) {
            return <Redirect to={'/dashboard'}/>
        }


        return (
            <React.Fragment>
                <InsuranceModal isOpen={this.state.modalIsOpen} carId={this.state.carId} coverage={this.state.coverage}
                                rate={this.state.rate}  cap={this.state.cap} hideModal={() => this.hideModal.bind(this)}/>
                <div className="row justify-content-center">
                    <div className=" col-md-10 bg-white form-container">
                        <form>
                            <div className={"form-group mt-5"}>
                                <h5 className="text-center">Check out our insuarnce packages.</h5>
                                <h1 className={"text-center pt-1"}>Each package brings inceadible functionality.</h1>
                                <p className="h6 text-center font-weight-bold pt-5">Select your insurance plan:</p>
                                <div className="card-deck mb-3 text-center">
                                    <div className="card mb-4 shadow-sm">
                                        <div className="card-header">
                                            <h4 className="my-0 font-weight-normal">Silver</h4>
                                        </div>
                                        <div className="card-body">
                                            <h1 className="card-title pricing-card-title">1000 miles
                                                <small
                                                    className="text-muted">/ 100$
                                                </small>
                                            </h1>
                                            <ul className="list-unstyled mt-3 mb-4">
                                                <li>Medical package</li>
                                                <li>Live data tracking</li>
                                                <li>Blockchain security</li>
                                                <li>Private assistance</li>
                                            </ul>
                                            <button type="button" onClick={() => this.showModal('silver', 0.02, 100)}
                                                    className="btn btn-lg btn-block btn-primary">Get
                                                started
                                            </button>
                                        </div>
                                    </div>
                                    <div className="card mb-4 shadow-sm">
                                        <div className="card-header">
                                            <h4 className="my-0 font-weight-normal">Gold</h4>
                                        </div>
                                        <div className="card-body">
                                            <h1 className="card-title pricing-card-title">5000 miles
                                                <small
                                                    className="text-muted">/ 500$
                                                </small>
                                            </h1>
                                            <ul className="list-unstyled mt-3 mb-4">
                                                <li>Medical package</li>
                                                <li>Live data tracking</li>
                                                <li>Blockchain security</li>
                                                <li>Private assistance</li>
                                            </ul>
                                            <button type="button" onClick={() => this.showModal('golden', 0.04, 500)}
                                                    className="btn btn-lg btn-block btn-primary">Get
                                                started
                                            </button>
                                        </div>
                                    </div>
                                    <div className="card mb-4 shadow-sm">
                                        <div className="card-header">
                                            <h4 className="my-0 font-weight-normal">Platinum</h4>
                                        </div>
                                        <div className="card-body">
                                            <h1 className="card-title pricing-card-title">9000 miles
                                                <small
                                                    className="text-muted">/ 1000$
                                                </small>
                                            </h1>
                                            <ul className="list-unstyled mt-3 mb-4">
                                                <li>Medical package</li>
                                                <li>Live data tracking</li>
                                                <li>Blockchain security</li>
                                                <li>Private assistance</li>
                                            </ul>
                                            <button type="button" onClick={() => this.showModal('platinum', 0.11, 1000)}
                                                    className="btn btn-lg btn-block btn-primary">Get
                                                started
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div className="row justify-content-center mt-5">
                                    <div onClick={this.onBack.bind(this)}
                                        className="btn btn-theme mb-5">Go back
                                    </div>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </React.Fragment>
        )
    }

}

export default withRouter(InsurancePage)