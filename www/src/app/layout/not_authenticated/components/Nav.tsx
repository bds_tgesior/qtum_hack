import * as React from 'react';
import {Link} from 'react-router-dom';
import LoginModal from "../../../login/components/LoginModal";

const qtumLogo = require('../../../../images/qtum_brand_logo.svg')

interface INavLiProps {
    name: string,
    style: string,
    to: string
}

const NavLi: React.SFC<INavLiProps> = (props) => {

    return (
        <li className={props.style}>
            <Link className="nav-link text-uppercase" to={props.to}>{props.name}</Link>
        </li>
    )
}

const NavList: React.SFC = () => {
    return (
        <ul className="navbar-nav ml-auto text-size--small">
            <NavLi name='Home' style='nav-item pr-5' to="/"/>
            <NavLi name='Register' style='nav-item pr-5' to="/register"/>
        </ul>
    )
}


interface INavLogoProps {
    img: any
}

const NavLogo: React.SFC<INavLogoProps> = (props) => {
    return (
        <Link className="navbar-brand" to={"/"}><img width="55px" src={props.img}/> Mondays insurance</Link>
    )
}

interface INavProps {

}

interface INavState {
    modalIsOpen: boolean
}

export default class Nav extends React.Component<INavProps, INavState> {

    constructor(props) {
        super(props)
        this.state = {
            modalIsOpen: false
        }
    }

    showModal() {
        this.setState({modalIsOpen: true})
    }

    hideModal() {
        this.setState({modalIsOpen: false})
    }

    public render() {


        return (
            <nav className="navbar navbar-expand-lg navbar-light">
                <LoginModal isOpen={this.state.modalIsOpen} hideModal={this.hideModal.bind(this)}  />
                <NavLogo img={qtumLogo}/>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                        aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"/>
                </button>
                <div className="collapse navbar-collapse" id="navbarNav">
                    <div className="top-menu-bg"/>
                    <NavList/>
                    <button onClick={() => this.showModal()}
                        className="btn btn-theme">login</button>
                </div>
            </nav>
        )
    }

}