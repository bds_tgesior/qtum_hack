export interface ApiUser {
    id: string
    name: string
    email: string
}