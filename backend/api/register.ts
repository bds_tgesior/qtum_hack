import {ApiRequest} from "./metadata";
import {NextFunction, Response} from "express";
import config from "../config";
import DB from "./aws/dynamoDB";
import HttpError from "./httpError";
import HttpStatus from "./httpStatus";
import * as uniqid from 'uniqid';
import * as bcrypt from 'bcrypt';

const UsersTableName = 'qtum-users'
const UsersAuthTableName = 'qtum-users-auth'

export async function register(req: ApiRequest, res: Response, next: NextFunction) {

    const {firstName, lastName, email, password} = req.body

    if (!firstName || !lastName || !email || !password) {
        return new next(new HttpError(HttpStatus.noContent, `Missing registration params.`))
    }

    try {
        const {aws: awsConfig} = await config()
        const db = new DB(awsConfig)

        const hash = await bcrypt.hash(password, 16.5)

        const userExists = await db.getItem(UsersTableName, {email})

        if (userExists) {
            return next(new HttpError(HttpStatus.conflict, `User with this email already exists.`))
        }

        const id = uniqid()

        await db.putItem(UsersTableName, {
            id,
            firstName,
            lastName,
            email,
            password: hash
        })

        await db.putItem(UsersAuthTableName, {
            id,
            email
        })

        res.send({status: HttpStatus.OK})

    } catch (err) {
        console.log(err)
        next(err)
    }

}