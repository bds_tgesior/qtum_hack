import {ApiRequest} from "../../metadata";
import * as Express from "express";
import HighMobility from "../../../lib/highMobility/api";

export async function start(req: ApiRequest, res: Express.Response, next: Express.NextFunction) {
    try {
        const HM = await HighMobility.create()

        const startCarResponse = await HM.startCar()
        res.send({startCarResponse})
    } catch(err) {
        console.log(err)
        next(err)
    }
}

export async function stop(req: ApiRequest, res: Express.Response, next: Express.NextFunction) {
    try {
        const HM = await HighMobility.create()

        const stopCarResponse = await HM.stopCar()
        res.send({stopCarResponse})
    } catch(err) {
        console.log(err)
        next(err)
    }
}

export async function engine(req: ApiRequest, res: Express.Response, next: Express.NextFunction) {
    try {
        const HM = await HighMobility.create()

        const getEngineStatusResponse = await HM.getEngineStatus()
        res.send({getEngineStatusResponse})
    } catch(err) {
        console.log(err)
        next(err)
    }
}
