import * as HMKit from 'hmkit'
import config from "../../config";


export default class HighMobility {

    private HMKit: HMKit
    private config

    constructor(config) {
        this.config = config
        this.HMKit = new HMKit(config.certificate, config.certificateHash)
    }

    static async create(): Promise<HighMobility> {
        const cfg = (await config()).highMobility

        return new HighMobility(cfg)
    }

    getInstance() {
        return this.HMKit
    }

    async accessCertificate(): Promise<any> {
        return await this.HMKit.downloadAccessCertificate(this.config.accessToken)
    }

    async startCar(): Promise<any> {
        const certificate = await this.accessCertificate()
        const HMKit = this.getInstance()
        return (await HMKit.telematics.sendCommand(
            certificate.getVehicleSerial(),
            HMKit.commands.EngineCommand.turnOn()
        )).parse()
    }

    async stopCar(): Promise<any> {
        const certificate = await this.accessCertificate()
        const HMKit = this.getInstance()
        return (await HMKit.telematics.sendCommand(
            certificate.getVehicleSerial(),
            HMKit.commands.EngineCommand.turnOff()
        )).parse()
    }

    async getEngineStatus(): Promise<any> {
        const certificate = await this.accessCertificate()
        const HMKit = this.getInstance()
        return (await HMKit.telematics.sendCommand(
            certificate.getVehicleSerial(),
            HMKit.commands.EngineCommand.getIgnitionState()
        )).parse()
    }

    async getDiagnostics(): Promise<any> {
        const certificate = await this.accessCertificate()
        const HMKit = this.getInstance()

        return (await HMKit.telematics.sendCommand(
            certificate.getVehicleSerial(),
            HMKit.commands.DiagnosticsCommand.getState()
        )).parse()
    }

    async getMaintenance(): Promise<any> {
        const certificate = await this.accessCertificate()
        const HMKit = this.getInstance()

        return (await HMKit.telematics.sendCommand(
            certificate.getVehicleSerial(),
            HMKit.commands.MaintenanceCommand.getState()
        )).parse()
    }

    async getCharging(): Promise<any> {
        const certificate = await this.accessCertificate()
        const HMKit = this.getInstance()

        return (await HMKit.telematics.sendCommand(
            certificate.getVehicleSerial(),
            // HMKit.commands.PowerTakeOffCommand.activate()
            HMKit.commands.ChargingCommand.getChargeState()
        )).parse()
    }

    async getClimate(): Promise<any> {
        const certificate = await this.accessCertificate()
        const HMKit = this.getInstance()

        return (await HMKit.telematics.sendCommand(
            certificate.getVehicleSerial(),
            HMKit.commands.ClimateCommand.getState()
        )).parse()
    }

    async getLocation(): Promise<any> {
        const certificate = await this.accessCertificate()
        const HMKit = this.getInstance()

        return (await HMKit.telematics.sendCommand(
            certificate.getVehicleSerial(),
            HMKit.commands.VehicleLocationCommand.getLocation()
        )).parse()
    }

    async getTrunkAccess(): Promise<any> {
        const certificate = await this.accessCertificate()
        const HMKit = this.getInstance()

        return (await HMKit.telematics.sendCommand(
            certificate.getVehicleSerial(),
            HMKit.commands.TrunkAccessCommand.getState()
        )).parse()
    }

    async getTheftAlarm(): Promise<any> {
        const certificate = await this.accessCertificate()
        const HMKit = this.getInstance()

        return (await HMKit.telematics.sendCommand(
            certificate.getVehicleSerial(),
            HMKit.commands.TheftAlarmCommand.getState()
        )).parse()
    }
    async getParkingBrake(): Promise<any> {
        const certificate = await this.accessCertificate()
        const HMKit = this.getInstance()

        return (await HMKit.telematics.sendCommand(
            certificate.getVehicleSerial(),
            HMKit.commands.ParkingBrakeCommand.getState()
        )).parse()
    }
    async getLightConditions(): Promise<any> {
        const certificate = await this.accessCertificate()
        const HMKit = this.getInstance()

        return (await HMKit.telematics.sendCommand(
            certificate.getVehicleSerial(),
            HMKit.commands.LightConditionsCommand.getConditions()
        )).parse()
    }
    async getLight(): Promise<any> {
        const certificate = await this.accessCertificate()
        const HMKit = this.getInstance()

        return (await HMKit.telematics.sendCommand(
            certificate.getVehicleSerial(),
            HMKit.commands.LightsCommand.getState()
        )).parse()
    }
}