import * as React from 'react';
import {Switch} from 'react-router';
import HomePage from "./home/containers/HomePage";
import AuthenticatedContainer from "./layout/authenticated/containers/AuthenticatedContainer";
import RegisterPage from "./register/containers/RegisterPage";
import {Route} from "react-router-dom";
import CarRegisterPage from "./car/containers/CarRegisterPage";
import NotAuthenticatedContainer from "./layout/not_authenticated/containers/NotAuthenticatedContainer";
import DashboardMainPage from "./dashboard/DashboardMainPage";
import {apiTokenStore, apiUserStore} from "./apiStore";
import InsurancePage from "./insurance/containers/InsurancePage";

function isAuthenticated() {
    return !!(apiTokenStore.get() && apiUserStore.get())
}


const Routes: React.SFC = () => {

    return (
        <Switch>
            <Route path="/" exact render={() => <NotAuthenticatedContainer isAuthenticated={isAuthenticated}><HomePage/></NotAuthenticatedContainer>}/>
            <Route path="/register"
                   render={() => <NotAuthenticatedContainer isAuthenticated={isAuthenticated}><RegisterPage/></NotAuthenticatedContainer>}/>
            <Route path={"/car-register"} render={() => <AuthenticatedContainer isAuthenticated={isAuthenticated}><CarRegisterPage/></AuthenticatedContainer>}/>
            <Route path={"/dashboard"} render={() => <AuthenticatedContainer isAuthenticated={isAuthenticated}><DashboardMainPage/></AuthenticatedContainer>}/>
            <Route path={"/insurance"} render={()=> <AuthenticatedContainer isAuthenticated={isAuthenticated}><InsurancePage/></AuthenticatedContainer>}/>
        </Switch>
    )

}

export default Routes