FROM node:8.11.1

ADD . /app

RUN chown -R node /app
USER node

RUN npm -q install

WORKDIR /app/backend
RUN npm -q install

CMD npm run start

EXPOSE 3001


