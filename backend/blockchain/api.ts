import {Contract, Qtum} from "qtumjs";

export default class QtumContract {

    private contract: Contract
    private qtum: Qtum

    constructor(qtum: Qtum, contractName: string) {
        this.qtum = qtum
        this.contract = this.qtum.contract(contractName)
    }

    static create(contractName) {

        const repoData = require("../../blockchain/solar.development.json")
        const qtum = new Qtum("http://test:test1234@18.195.205.107:8332", repoData)

        return new QtumContract(qtum, contractName)
    }

    getInstance() {
        return this.contract
    }

}
