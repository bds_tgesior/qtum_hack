import cloudform from 'cloudform'
import {
    ApplicationAutoScaling,
    AutoScaling,
    CloudWatch,
    DeletionPolicy,
    EC2,
    ECS,
    ElasticLoadBalancingV2,
    IAM,
    RDS,
    Fn,
    Logs,
    Refs,
    ResourceTag,
    S3,
    StringParameter
} from 'cloudform'

const ProjectName = 'TODO'

const Resources = {
    // networking
    HttpHttpsServerSecurityGroup: 'HttpHttpsServerSecurityGroup',
    AccessInternetSecurityGroup: 'AccessInternetSecurityGroup',
    VPC: 'VPC',
    PublicASubnet: 'PublicASubnet',
    PublicBSubnet: 'PublicBSubnet',
    InternetGateway: 'InternetGateway',
    PublicRouteTable: 'PublicRouteTable',
    PublicRoute: 'PublicRoute',
    PublicASubnetRouteTableAssociation: 'PublicASubnetRouteTableAssociation',
    PublicBSubnetRouteTableAssociation: 'PublicBSubnetRouteTableAssociation',
    VPCGatewayToInternetAttachment: 'VPCGatewayToInternetAttachment',
    PrivateARouteTable: 'PrivateARouteTable',
    PrivateBRouteTable: 'PrivateBRouteTable',
    PrivateASubnetRouteTableAssociation: 'PrivateASubnetRouteTableAssociation',
    PrivateBSubnetRouteTableAssociation: 'PrivateBSubnetRouteTableAssociation',
    PrivateASubnet: 'PrivateASubnet',
    PrivateBSubnet: 'PrivateBSubnet',
    NatGatewayEIP: 'NatGatewayEIP',
    NatGateway: 'NatGateway',
    PrivateARouteNat: 'PrivateARouteNat',
    PrivateBRouteNat: 'PrivateBRouteNat',

    // database
    BackendDBSubnetGroup: 'BackendDBSubnetGroup',
    BackendDBInstanceParameters: 'BackendDBInstanceParameters',
    BackendDBInstance: 'BackendDBInstance',
    BackendDBEC2SecurityGroup: 'BackendDBEC2SecurityGroup',

    // bastion host
    BastionIPAddress: 'BastionIPAddress',
    BastionHost: 'BastionHost',
    BastionSecurityGroup: 'BastionSecurityGroup',

    // storage
    UploadsBucket: 'UploadsBucket',

    // services access
    AppApiS3AccessPolicy: 'AppApiS3AccessPolicy',
    AppApiSESAccessPolicy: 'AppApiSESAccessPolicy',
    AppApiAccessUser: 'AppApiAccessUser',
    AppApiAccessCredentials: 'AppApiAccessCredentials',

    // logs
    CloudwatchLogsGroup: 'CloudwatchLogsGroup',

    // ECS
    ECSCluster: 'ECSCluster',
    ECSSecurityGroup: 'ECSSecurityGroup',
    ECSSecurityGroupALBports: 'ECSSecurityGroupALBports',
    TaskDefinition: 'TaskDefinition',
    ContainerInstances: 'ContainerInstances',
    ECSService: 'ECSService',

    // load balancer
    ECSALB: 'ECSALB',
    ALBHttpsListener: 'ALBHttpsListener',
    ALBHttpListener: 'ALBHttpListener',
    ECSALBListenerRule: 'ECSALBListenerRule',
    ECSTargetGroup: 'ECSTargetGroup',
    ECSAutoScalingGroup: 'ECSAutoScalingGroup',
    ECSServiceRole: 'ECSServiceRole',

    // auto scaling
    ServiceScalingTarget: 'ServiceScalingTarget',
    ServiceScalingPolicy: 'ServiceScalingPolicy',
    CPUUsageAlarmScaleUp: 'CPUUsageAlarmScaleUp',
    EC2Role: 'EC2Role',
    AutoscalingRole: 'AutoscalingRole',
    EC2InstanceProfile: 'EC2InstanceProfile'
}

const DeployEnv = Fn.Ref('DeployEnv')

const InternetAccessSecurity = [
    {
        IpProtocol: "tcp",
        FromPort: 80,
        ToPort: 80,
        CidrIp: "0.0.0.0/0"
    },
    {
        IpProtocol: "tcp",
        FromPort: 443,
        ToPort: 443,
        CidrIp: "0.0.0.0/0"
    }
]

export default cloudform({
    Parameters: {
        DeployEnv: new StringParameter({
            Description: "Deploy environment name",
            AllowedValues: ["stage", "production"]
        }),
        AppImage: new StringParameter({
            Description: "Repository, image and tag of the app to deploy",
        }),
        BackendDbPassword: new StringParameter({
            Description: "Password for database",
            NoEcho: true
        }),
        SSHFrom: new StringParameter({
            Description: "Lockdown SSH access to the bastion host (default can be accessed from anywhere)",
            MinLength: 9,
            MaxLength: 18,
            Default: "0.0.0.0/0",
            AllowedPattern: "(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})/(\\d{1,2})",
            ConstraintDescription: "must be a valid CIDR range of the form x.x.x.x/x."
        })
    },
    Mappings: {
        SubnetConfig: {
            VPC: {
                CIDR: "10.0.0.0/16"
            },
            PrivateA: {
                CIDR: "10.0.0.0/24"
            },
            PrivateB: {
                CIDR: "10.0.1.0/24"
            },
            PublicA: {
                CIDR: "10.0.100.0/24"
            },
            PublicB: {
                CIDR: "10.0.101.0/24"
            }
        },
        DatabaseMapping: {
            production: {
                BackendDBInstanceIdentifier: `${ProjectName}-production`,
                BackendInstanceType: "db.t2.micro",
                BackendStorageType: "gp2",
                BackendAllocatedStorage: 10,
                DbName: ProjectName,
                DbUserName: ProjectName,
                MultiAZ: "true"
            },
            stage: {
                BackendDBInstanceIdentifier: `${ProjectName}-stage`,
                BackendInstanceType: "db.t2.micro",
                BackendStorageType: "gp2",
                BackendAllocatedStorage: 10,
                DbName: ProjectName,
                DbUserName: ProjectName,
                MultiAZ: "false"
            }
        },
        ECS: {
            stage: {
                InstanceType: "t2.micro",
                ContainerName: `${ProjectName}-www-stage`,
                ContainerPort: "3001",
                Memory: "900",
                DesiredTasksCount: 1
            },
            production: {
                InstanceType: "t2.small",
                ContainerName: `${ProjectName}-www-production`,
                ContainerPort: "3001",
                Memory: "900",
                DesiredTasksCount: 1
            }
        },
        Certificates: {
            stage: {
                ARN: "arn:aws:acm:eu-central-1:TODO"
            },
            production: {
                ARN: "arn:aws:acm:eu-central-1:TODO"
            }
        }
    },
    Resources: {
        [Resources.HttpHttpsServerSecurityGroup]: new EC2.SecurityGroup({
            GroupDescription: "Enables inbound HTTP and HTTPS access via port 80 and 443",
            VpcId: Fn.Ref(Resources.VPC),
            SecurityGroupIngress: InternetAccessSecurity
        }),

        [Resources.AccessInternetSecurityGroup]: new EC2.SecurityGroup({
            GroupDescription: "Enables outbound Internet access via ports 80,443",
            VpcId: Fn.Ref(Resources.VPC),
            SecurityGroupEgress: InternetAccessSecurity
        }),

        [Resources.VPC]: new EC2.VPC({
            CidrBlock: Fn.FindInMap('SubnetConfig', Resources.VPC, 'CIDR'),
            EnableDnsHostnames: true,
            Tags: [
                new ResourceTag("Application", Refs.StackName),
                new ResourceTag("Network", "Public"),
                new ResourceTag("Name", Fn.Join('-', [Refs.StackName, Resources.VPC]))
            ]
        }),

        [Resources.PublicASubnet]: new EC2.Subnet({
            VpcId: Fn.Ref(Resources.VPC),
            AvailabilityZone: Fn.Select(0, Fn.GetAZs()),
            CidrBlock: Fn.FindInMap('SubnetConfig', 'PublicA', 'CIDR'),
            Tags: [
                new ResourceTag("Application", Refs.StackName),
                new ResourceTag("Network", "Public"),
                new ResourceTag("Name", Fn.Join('-', [Refs.StackName, Resources.PublicASubnet]))
            ]
        }).dependsOn(Resources.VPC),

        [Resources.PublicBSubnet]: new EC2.Subnet({
            VpcId: Fn.Ref(Resources.VPC),
            AvailabilityZone: Fn.Select(1, Fn.GetAZs()),
            CidrBlock: Fn.FindInMap('SubnetConfig', 'PublicB', 'CIDR'),
            Tags: [
                new ResourceTag("Application", Refs.StackName),
                new ResourceTag("Network", "Public"),
                new ResourceTag("Name", Fn.Join('-', [Refs.StackName, Resources.PublicBSubnet]))
            ]
        }).dependsOn(Resources.VPC),

        [Resources.InternetGateway]: new EC2.InternetGateway({
            Tags: [
                new ResourceTag('Application', Refs.StackName),
                new ResourceTag('Network', 'Public'),
                new ResourceTag('Name', Fn.Join('-', [Refs.StackName, Resources.InternetGateway]))
            ]
        }),

        [Resources.PublicRouteTable]: new EC2.RouteTable({
            VpcId: Fn.Ref(Resources.VPC),
            Tags: [
                new ResourceTag('Application', Refs.StackName),
                new ResourceTag('Network', 'Public'),
                new ResourceTag('Name', Fn.Join('-', [Refs.StackName, Resources.PublicRouteTable]))
            ]
        }).dependsOn(Resources.VPC),

        [Resources.PublicRoute]: new EC2.Route({
            RouteTableId: Fn.Ref(Resources.PublicRouteTable),
            DestinationCidrBlock: "0.0.0.0/0",
            GatewayId: Fn.Ref(Resources.InternetGateway)
        }).dependsOn([
            Resources.PublicRouteTable,
            Resources.InternetGateway
        ]),

        [Resources.PublicASubnetRouteTableAssociation]: new EC2.SubnetRouteTableAssociation({
            SubnetId: Fn.Ref(Resources.PublicASubnet),
            RouteTableId: Fn.Ref(Resources.PublicRouteTable)
        }).dependsOn([
            Resources.PublicASubnet,
            Resources.PublicRouteTable
        ]),

        [Resources.PublicBSubnetRouteTableAssociation]: new EC2.SubnetRouteTableAssociation({
            SubnetId: Fn.Ref(Resources.PublicBSubnet),
            RouteTableId: Fn.Ref(Resources.PublicRouteTable)
        }).dependsOn([
            Resources.PublicBSubnet,
            Resources.PublicRouteTable
        ]),

        [Resources.VPCGatewayToInternetAttachment]: new EC2.VPCGatewayAttachment({
            VpcId: Fn.Ref(Resources.VPC),
            InternetGatewayId: Fn.Ref(Resources.InternetGateway)
        }).dependsOn([
            Resources.VPC,
            Resources.InternetGateway
        ]),

        [Resources.PrivateARouteTable]: new EC2.RouteTable({
            VpcId: Fn.Ref(Resources.VPC),
            Tags: [
                new ResourceTag('Application', Refs.StackName),
                new ResourceTag('Network', 'Private'),
                new ResourceTag('Name', Fn.Join('-', [Refs.StackName, Resources.PrivateARouteTable]))
            ]
        }),

        [Resources.PrivateBRouteTable]: new EC2.RouteTable({
            VpcId: Fn.Ref(Resources.VPC),
            Tags: [
                new ResourceTag('Application', Refs.StackName),
                new ResourceTag('Network', 'Private'),
                new ResourceTag('Name', Fn.Join('-', [Refs.StackName, Resources.PrivateBRouteTable]))
            ]
        }),

        [Resources.PrivateASubnetRouteTableAssociation]: new EC2.SubnetRouteTableAssociation({
            SubnetId: Fn.Ref(Resources.PrivateASubnet),
            RouteTableId: Fn.Ref(Resources.PrivateARouteTable)
        }).dependsOn([
            Resources.PrivateASubnet,
            Resources.PrivateARouteTable
        ]),

        [Resources.PrivateBSubnetRouteTableAssociation]: new EC2.SubnetRouteTableAssociation({
            SubnetId: Fn.Ref(Resources.PrivateBSubnet),
            RouteTableId: Fn.Ref(Resources.PrivateBRouteTable)
        }).dependsOn([
            Resources.PrivateBSubnet,
            Resources.PrivateBRouteTable
        ]),

        [Resources.PrivateASubnet]: new EC2.Subnet({
            VpcId: Fn.Ref(Resources.VPC),
            AvailabilityZone: Fn.Select(0, Fn.GetAZs()),
            CidrBlock: Fn.FindInMap('SubnetConfig', 'PrivateA', 'CIDR'),
            Tags: [
                new ResourceTag('Application', Refs.StackName),
                new ResourceTag('Network', 'Private'),
                new ResourceTag('Name', Fn.Join('-', [Refs.StackName, Resources.PrivateASubnet]))
            ]
        }).dependsOn(Resources.VPC),

        [Resources.PrivateBSubnet]: new EC2.Subnet({
            VpcId: Fn.Ref(Resources.VPC),
            AvailabilityZone: Fn.Select(1, Fn.GetAZs()),
            CidrBlock: Fn.FindInMap('SubnetConfig', 'PrivateB', 'CIDR'),
            Tags: [
                new ResourceTag('Application', Refs.StackName),
                new ResourceTag('Network', 'Private'),
                new ResourceTag('Name', Fn.Join('-', [Refs.StackName, Resources.PrivateBSubnet]))
            ]
        }).dependsOn(Resources.VPC),

        [Resources.NatGatewayEIP]: new EC2.EIP({
            Domain: "vpc"
        }).dependsOn(Resources.VPCGatewayToInternetAttachment),

        [Resources.NatGateway]: new EC2.NatGateway({
            AllocationId: Fn.GetAtt(Resources.NatGatewayEIP, 'AllocationId'),
            SubnetId: Fn.Ref(Resources.PublicASubnet)
        }),

        [Resources.PrivateARouteNat]: new EC2.Route({
            RouteTableId: Fn.Ref(Resources.PrivateARouteTable),
            DestinationCidrBlock: "0.0.0.0/0",
            NatGatewayId: Fn.Ref(Resources.NatGateway)
        }),

        [Resources.PrivateBRouteNat]: new EC2.Route({
            RouteTableId: Fn.Ref(Resources.PrivateBRouteTable),
            DestinationCidrBlock: "0.0.0.0/0",
            NatGatewayId: Fn.Ref(Resources.NatGateway)
        }),

        [Resources.BackendDBSubnetGroup]: new RDS.DBSubnetGroup({
            DBSubnetGroupDescription: "Backend db subnet group that allows for multi-az",
            SubnetIds: [
                Fn.Ref(Resources.PrivateASubnet),
                Fn.Ref(Resources.PrivateBSubnet)
            ]
        }),

        [Resources.BackendDBInstanceParameters]: new RDS.DBParameterGroup({
            Description: "Backend DB Instance Parameter Group",
            Family: "postgres9.6",
            Tags: [
                new ResourceTag('Application', Refs.StackName),
                new ResourceTag('Name', Fn.Join('-', [Refs.StackName, Resources.BackendDBInstance]))
            ]
        }),

        [Resources.BackendDBInstance]: new RDS.DBInstance({
            DBName: Fn.FindInMap('DatabaseMapping', DeployEnv, 'DbName'),
            MasterUsername: Fn.FindInMap('DatabaseMapping', DeployEnv, 'DbUserName'),
            DBInstanceIdentifier: Fn.FindInMap('DatabaseMapping', DeployEnv, 'BackendDBInstanceIdentifier'),
            DBParameterGroupName: Fn.Ref(Resources.BackendDBInstanceParameters),
            DBSubnetGroupName: Fn.Ref(Resources.BackendDBSubnetGroup),
            MasterUserPassword: Fn.Ref('BackendDbPassword'),
            Engine: "postgres",
            EngineVersion: "9.6.1",
            AllowMajorVersionUpgrade: true,
            DBInstanceClass: Fn.FindInMap('DatabaseMapping', DeployEnv, 'BackendInstanceType'),
            VPCSecurityGroups: [
                Fn.GetAtt(Resources.BackendDBEC2SecurityGroup, 'GroupId')
            ],
            AllocatedStorage: Fn.FindInMap('DatabaseMapping', DeployEnv, 'BackendAllocatedStorage'),
            StorageType: Fn.FindInMap('DatabaseMapping', DeployEnv, 'BackendStorageType'),
            MultiAZ: Fn.FindInMap('DatabaseMapping', DeployEnv, 'MultiAZ'),
            Tags: [
                new ResourceTag('Application', Refs.StackName),
                new ResourceTag('Name', Fn.Join('-', [Refs.StackName, Resources.BackendDBInstance]))
            ]
        }).deletionPolicy(DeletionPolicy.Snapshot),

        [Resources.BackendDBEC2SecurityGroup]: new EC2.SecurityGroup({
            GroupDescription: "Open database for access",
            VpcId: Fn.Ref(Resources.VPC),
            SecurityGroupIngress: [
                {
                    IpProtocol: "tcp",
                    FromPort: 5432,
                    ToPort: 5432,
                    CidrIp: Fn.FindInMap('SubnetConfig', 'PublicA', 'CIDR')
                },
                {
                    IpProtocol: "tcp",
                    FromPort: 5432,
                    ToPort: 5432,
                    CidrIp: Fn.FindInMap('SubnetConfig', 'PublicB', 'CIDR')
                },
                {
                    IpProtocol: "tcp",
                    FromPort: 5432,
                    ToPort: 5432,
                    CidrIp: Fn.FindInMap('SubnetConfig', 'PrivateA', 'CIDR')
                },
                {
                    IpProtocol: "tcp",
                    FromPort: 5432,
                    ToPort: 5432,
                    CidrIp: Fn.FindInMap('SubnetConfig', 'PrivateB', 'CIDR')
                }
            ]
        }),

        [Resources.BastionIPAddress]: new EC2.EIP({
            Domain: "vpc",
            InstanceId: Fn.Ref(Resources.BastionHost)
        }),

        [Resources.BastionHost]: new EC2.Instance({
            InstanceType: "t2.nano",
            KeyName: 'ssh-access',
            ImageId: "ami-ac442ac3",
            NetworkInterfaces: [
                {
                    AssociatePublicIpAddress: true,
                    DeviceIndex: "0",
                    SubnetId: Fn.Ref(Resources.PublicASubnet),
                    DeleteOnTermination: true,
                    GroupSet: [
                        Fn.Ref(Resources.BastionSecurityGroup)
                    ]
                }
            ],
            Tags: [
                new ResourceTag('Application', Refs.StackName),
                new ResourceTag('Name', Resources.BastionHost)
            ]
        }).dependsOn([
            Resources.PublicASubnet,
            Resources.BastionSecurityGroup
        ]),

        [Resources.BastionSecurityGroup]: new EC2.SecurityGroup({
            GroupDescription: "Enable access to the Bastion host",
            VpcId: Fn.Ref(Resources.VPC),
            SecurityGroupIngress: [
                {
                    IpProtocol: "tcp",
                    FromPort: 22,
                    ToPort: 22,
                    CidrIp: Fn.Ref('SSHFrom')
                }
            ]
        }).dependsOn(Resources.VPC),

        [Resources.UploadsBucket]: new S3.Bucket({
            AccessControl: "PublicRead",
            BucketName: Fn.Join('-', [ProjectName, 'uploads', DeployEnv]),
            LifecycleConfiguration: {
                Rules: [
                    {
                        ExpirationInDays: 1,
                        Status: "Enabled"
                    }
                ]
            }
        }),

        [Resources.AppApiS3AccessPolicy]: new IAM.ManagedPolicy({
            PolicyDocument: {
                Version: "2012-10-17",
                Statement: [
                    {
                        Effect: "Allow",
                        Action: "s3:ListAllMyBuckets",
                        Resource: "arn:aws:s3:::*"
                    },
                    {
                        Effect: "Allow",
                        Action: "s3:*",
                        Resource: [
                            Fn.Join('', ["arn:aws:s3:::", ProjectName, "-uploads-", DeployEnv]),
                            Fn.Join('', ["arn:aws:s3:::", ProjectName, "-uploads-", DeployEnv, '/*'])
                        ]
                    }
                ]
            }
        }),

        [Resources.AppApiSESAccessPolicy]: new IAM.ManagedPolicy({
            PolicyDocument: {
                Version: "2012-10-17",
                Statement: [
                    {
                        Effect: "Allow",
                        Action: [
                            "ses:SendEmail",
                            "ses:SendRawEmail"
                        ],
                        Resource: Fn.Join('', ['arn:aws:ses:eu-west-1:', Refs.AccountId, ':*'])
                    }
                ]
            }
        }),

        [Resources.AppApiAccessUser]: new IAM.User({
            ManagedPolicyArns: [
                Fn.Ref(Resources.AppApiS3AccessPolicy),
                Fn.Ref(Resources.AppApiSESAccessPolicy)
            ]
        }),

        [Resources.AppApiAccessCredentials]: new IAM.AccessKey({
            UserName: Fn.Ref(Resources.AppApiAccessUser)
        }),

        [Resources.ECSCluster]: new ECS.Cluster(),

        [Resources.ECSSecurityGroup]: new EC2.SecurityGroup({
            GroupDescription: "ECS Security Group",
            VpcId: Fn.Ref(Resources.VPC)
        }),

        [Resources.ECSSecurityGroupALBports]: new EC2.SecurityGroupIngress({
            GroupId: Fn.Ref('ECSSecurityGroup'),
            IpProtocol: "tcp",
            FromPort: 31000,
            ToPort: 61000,
            CidrIp: Fn.FindInMap('SubnetConfig', Resources.VPC, 'CIDR')
        }),

        [Resources.CloudwatchLogsGroup]: new Logs.LogGroup({
            LogGroupName: Fn.Join('-', ['ECSLogGroup', Refs.StackName]),
            RetentionInDays: 14
        }),

        [Resources.TaskDefinition]: new ECS.TaskDefinition({
            Family: Fn.Join('', [Refs.StackName, '-app']),
            ContainerDefinitions: [
                {
                    Name: Fn.FindInMap('ECS', DeployEnv, 'ContainerName'),
                    Environment: [
                        {
                            Name: "DB_URL",
                            Value: Fn.Join('', [
                                "postgresql://",
                                Fn.FindInMap('DatabaseMapping', DeployEnv, 'DbUserName'),
                                ":",
                                Fn.Ref('BackendDbPassword'),
                                "@",
                                Fn.GetAtt(Resources.BackendDBInstance, 'Endpoint.Address'),
                                "/",
                                Fn.FindInMap('DatabaseMapping', DeployEnv, 'DbName')
                            ])
                        },
                        {
                            Name: "NODE_ENV",
                            Value: DeployEnv
                        },
                        {
                            Name: "JWT_SECRET",
                            Value: Fn.Base64(Fn.Join("@#$", [DeployEnv, Fn.Ref('BackendDbPassword')]))
                        },
                        {
                            Name: "AWS_ACCESS_KEY_ID",
                            Value: Fn.Ref(Resources.AppApiAccessCredentials)
                        },
                        {
                            Name: "AWS_SECRET_ACCESS_KEY",
                            Value: Fn.GetAtt(Resources.AppApiAccessCredentials, 'SecretAccessKey')
                        }
                    ],
                    Cpu: 2,
                    Essential: true,
                    Image: Fn.Ref('AppImage'),
                    MemoryReservation: Fn.FindInMap('ECS', DeployEnv, 'Memory'),
                    LogConfiguration: {
                        LogDriver: "awslogs",
                        Options: {
                            "awslogs-group": Fn.Ref('CloudwatchLogsGroup'),
                            "awslogs-region": Refs.Region,
                            "awslogs-stream-prefix": Fn.Select(1, Fn.Split(':', Fn.Ref('AppImage'))),
                            "awslogs-multiline-pattern": "^[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}.[0-9]{3}Z"
                        }
                    },
                    PortMappings: [
                        {
                            ContainerPort: Fn.FindInMap('ECS', DeployEnv, 'ContainerPort')
                        }
                    ]
                }
            ]
        }),

        [Resources.ECSALB]: new ElasticLoadBalancingV2.LoadBalancer({
            Name: Fn.Join('-', [Refs.StackName, Resources.ECSALB]),
            Scheme: "internet-facing",
            LoadBalancerAttributes: [
                {
                    Key: "idle_timeout.timeout_seconds",
                    Value: "30"
                }
            ],
            Subnets: [
                Fn.Ref(Resources.PublicASubnet),
                Fn.Ref(Resources.PublicBSubnet)
            ],
            SecurityGroups: [
                Fn.Ref(Resources.HttpHttpsServerSecurityGroup)
            ]
        }).dependsOn(Resources.HttpHttpsServerSecurityGroup),

        [Resources.ALBHttpsListener]: new ElasticLoadBalancingV2.Listener({
            Certificates: [
                {
                    CertificateArn: Fn.FindInMap('Certificates', DeployEnv, 'ARN')
                }
            ],
            DefaultActions: [
                {
                    Type: "forward",
                    TargetGroupArn: Fn.Ref(Resources.ECSTargetGroup)
                }
            ],
            LoadBalancerArn: Fn.Ref(Resources.ECSALB),
            Port: 443,
            Protocol: "HTTPS"
        }).dependsOn(Resources.ECSServiceRole),

        [Resources.ALBHttpListener]: new ElasticLoadBalancingV2.Listener({
            DefaultActions: [
                {
                    Type: "forward",
                    TargetGroupArn: Fn.Ref(Resources.ECSTargetGroup)
                }
            ],
            LoadBalancerArn: Fn.Ref(Resources.ECSALB),
            Port: 80,
            Protocol: "HTTP"
        }).dependsOn(Resources.ECSServiceRole),

        [Resources.ECSALBListenerRule]: new ElasticLoadBalancingV2.ListenerRule({
            Actions: [
                {
                    Type: "forward",
                    TargetGroupArn: Fn.Ref(Resources.ECSTargetGroup)
                }
            ],
            Conditions: [
                {
                    Field: "path-pattern",
                    Values: ["/"]
                }
            ],
            ListenerArn: Fn.Ref(Resources.ALBHttpsListener),
            Priority: 1
        }).dependsOn(Resources.ALBHttpsListener),

        [Resources.ECSTargetGroup]: new ElasticLoadBalancingV2.TargetGroup({
            HealthCheckIntervalSeconds: 10,
            HealthCheckPath: "/health",
            HealthCheckProtocol: "HTTP",
            HealthCheckTimeoutSeconds: 5,
            HealthyThresholdCount: 2,
            Name: Fn.Join('-', [Resources.ECSTargetGroup, DeployEnv]),
            Port: Fn.FindInMap('ECS', DeployEnv, 'ContainerPort'),
            Protocol: "HTTP",
            TargetGroupAttributes: [
                {
                    Key: "deregistration_delay.timeout_seconds",
                    Value: "30"
                },
                {
                    Key: "stickiness.enabled",
                    Value: "true"
                },
                {
                    Key: "stickiness.type",
                    Value: "lb_cookie"
                },
                {
                    Value: "86400",
                    Key: "stickiness.lb_cookie.duration_seconds"
                }
            ],
            UnhealthyThresholdCount: 2,
            VpcId: Fn.Ref(Resources.VPC)
        }).dependsOn(Resources.ECSALB),

        [Resources.ECSAutoScalingGroup]: new AutoScaling.AutoScalingGroup({
            VPCZoneIdentifier: [
                Fn.Ref(Resources.PrivateASubnet),
                Fn.Ref(Resources.PrivateBSubnet)
            ],
            LaunchConfigurationName: Fn.Ref(Resources.ContainerInstances),
            MinSize: "2",
            MaxSize: "6",
            DesiredCapacity: "2"
        }).creationPolicy({
            ResourceSignal: {
                Timeout: "PT5M"
            }
        }).updatePolicy({
            AutoScalingReplacingUpdate: {
                WillReplace: true
            }
        }),

        [Resources.ContainerInstances]: new AutoScaling.LaunchConfiguration({
            ImageId: "ami-3b7d1354",
            SecurityGroups: [
                Fn.Ref(Resources.ECSSecurityGroup)
            ],
            InstanceType: Fn.FindInMap('ECS', DeployEnv, 'InstanceType'),
            IamInstanceProfile: Fn.Ref(Resources.EC2InstanceProfile),
            KeyName: 'ssh-access',
            UserData: Fn.Base64(Fn.Join('', [
                "#!/bin/bash -xe\n",
                "echo ECS_CLUSTER=",
                Fn.Ref(Resources.ECSCluster),
                " >> /etc/ecs/ecs.config\n",
                "yum install -y aws-cfn-bootstrap\n",
                "/opt/aws/bin/cfn-signal -e $? ",
                "         --stack ",
                Refs.StackName,
                "         --resource ECSAutoScalingGroup ",
                "         --region ",
                Refs.Region,
                "\n"
            ]))
        }).dependsOn([
            Resources.ECSSecurityGroup,
            Resources.PublicRoute
        ]),

        [Resources.ECSService]: new ECS.Service({
            Cluster: Fn.Ref(Resources.ECSCluster),
            DesiredCount: Fn.FindInMap('ECS', DeployEnv, 'DesiredTasksCount'),
            LoadBalancers: [
                {
                    ContainerName: Fn.FindInMap('ECS', DeployEnv, 'ContainerName'),
                    ContainerPort: Fn.FindInMap('ECS', DeployEnv, 'ContainerPort'),
                    TargetGroupArn: Fn.Ref(Resources.ECSTargetGroup)
                }
            ],
            DeploymentConfiguration: {
                MinimumHealthyPercent: 50
            },
            Role: Fn.Ref(Resources.ECSServiceRole),
            TaskDefinition: Fn.Ref(Resources.TaskDefinition)
        }).dependsOn(Resources.ALBHttpsListener),

        [Resources.ECSServiceRole]: new IAM.Role({
            AssumeRolePolicyDocument: {
                Statement: [
                    {
                        Effect: "Allow",
                        Principal: {
                            Service: ["ecs.amazonaws.com"]
                        },
                        Action: ["sts:AssumeRole"]
                    }
                ]
            },
            Path: "/",
            Policies: [
                {
                    PolicyName: "ecs-service",
                    PolicyDocument: {
                        Statement: [
                            {
                                Effect: "Allow",
                                Action: [
                                    "elasticloadbalancing:DeregisterInstancesFromLoadBalancer",
                                    "elasticloadbalancing:DeregisterTargets",
                                    "elasticloadbalancing:Describe*",
                                    "elasticloadbalancing:RegisterInstancesWithLoadBalancer",
                                    "elasticloadbalancing:RegisterTargets",
                                    "ec2:Describe*",
                                    "ec2:AuthorizeSecurityGroupIngress"
                                ],
                                Resource: "*"
                            }
                        ]
                    }
                }
            ]
        }),

        [Resources.ServiceScalingTarget]: new ApplicationAutoScaling.ScalableTarget({
            MaxCapacity: 2,
            MinCapacity: 1,
            ResourceId: Fn.Join('', [
                "service/",
                Fn.Ref(Resources.ECSCluster),
                "/",
                Fn.GetAtt(Resources.ECSService, 'Name')
            ]),
            RoleARN: Fn.GetAtt(Resources.AutoscalingRole, 'Arn'),
            ScalableDimension: "ecs:service:DesiredCount",
            ServiceNamespace: "ecs"
        }).dependsOn(Resources.ECSService),

        [Resources.ServiceScalingPolicy]: new ApplicationAutoScaling.ScalingPolicy({
            PolicyName: "AStepPolicy",
            PolicyType: "StepScaling",
            ScalingTargetId: Fn.Ref(Resources.ServiceScalingTarget),
            StepScalingPolicyConfiguration: {
                AdjustmentType: "PercentChangeInCapacity",
                Cooldown: 60,
                MetricAggregationType: "Average",
                StepAdjustments: [
                    {
                        MetricIntervalLowerBound: 0,
                        ScalingAdjustment: 200
                    }
                ]
            }
        }),

        [Resources.CPUUsageAlarmScaleUp]: new CloudWatch.Alarm({
            AlarmDescription: "Alarm if instance CPU usage is >75%.",
            AlarmActions: [Fn.Ref(Resources.ServiceScalingPolicy)],
            MetricName: "CPUUtilization",
            Namespace: "AWS/EC2",
            Statistic: "Average",
            Period: 60,
            EvaluationPeriods: 3,
            Threshold: 75,
            ComparisonOperator: "GreaterThanThreshold",
            Dimensions: [
                {
                    Name: "AutoScalingGroupName",
                    Value: Fn.Ref(Resources.ECSAutoScalingGroup)
                }
            ]
        }),

        [Resources.EC2Role]: new IAM.Role({
            AssumeRolePolicyDocument: {
                Statement: [
                    {
                        Effect: "Allow",
                        Principal: {
                            Service: ["ec2.amazonaws.com"]
                        },
                        Action: ["sts:AssumeRole"]
                    }
                ]
            },
            Path: "/",
            Policies: [
                {
                    PolicyName: "ecs-service",
                    PolicyDocument: {
                        Statement: [
                            {
                                Effect: "Allow",
                                Action: [
                                    "ecs:CreateCluster",
                                    "ecs:DeregisterContainerInstance",
                                    "ecs:DiscoverPollEndpoint",
                                    "ecs:Poll",
                                    "ecs:RegisterContainerInstance",
                                    "ecs:StartTelemetrySession",
                                    "ecs:Submit*",
                                    "ecr:GetAuthorizationToken",
                                    "ecr:BatchCheckLayerAvailability",
                                    "ecr:GetDownloadUrlForLayer",
                                    "ecr:BatchGetImage",
                                    "logs:CreateLogStream",
                                    "logs:PutLogEvents"
                                ],
                                Resource: "*"
                            }
                        ]
                    }
                }
            ]
        }),

        [Resources.AutoscalingRole]: new IAM.Role({
            AssumeRolePolicyDocument: {
                Statement: [
                    {
                        Effect: "Allow",
                        Principal: {
                            Service: ["application-autoscaling.amazonaws.com"]
                        },
                        Action: ["sts:AssumeRole"]
                    }
                ]
            },
            Path: "/",
            Policies: [
                {
                    PolicyName: "service-autoscaling",
                    PolicyDocument: {
                        Statement: [
                            {
                                Effect: "Allow",
                                Action: [
                                    "application-autoscaling:*",
                                    "cloudwatch:DescribeAlarms",
                                    "cloudwatch:PutMetricAlarm",
                                    "ecs:DescribeServices",
                                    "ecs:UpdateService"
                                ],
                                Resource: "*"
                            }
                        ]
                    }
                }
            ]
        }),

        [Resources.EC2InstanceProfile]: new IAM.InstanceProfile({
            Path: "/",
            Roles: [Fn.Ref(Resources.EC2Role)]
        })
    }
})
