import {Response, NextFunction} from 'express'

import {ApiRequest} from '../metadata'
import {ApiUser, prepareToken} from '../auth'
import HttpStatus from "../httpStatus"

class AuthUserResponse {
    id: string
    email?: string
    // TODO add more fields as needed; replace ApiUser with more specific model if needed

    constructor(user: ApiUser) {
        this.id = user.id!
        this.email = user.email
    }
}

class AuthLoginResponse {
    apiToken: string
    user: AuthUserResponse

    constructor(apiToken: string, user: ApiUser) {
        this.apiToken = apiToken
        this.user = new AuthUserResponse(user)
    }
}

export function login(req: ApiRequest, res: Response) {
    const user = req.user!
    const apiToken = prepareToken({id: user.id, role: user.role})
    res.send(new AuthLoginResponse(apiToken, user))
}

