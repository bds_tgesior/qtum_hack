import {post,get} from "../apiClient";

export interface Car {
    vin?: string | undefined
    carCompany: string
    carModel: string
    mileage?: number | undefined
    productionYear: number | undefined
    carId?:string
    insurance?:any
}

export async function register(car: any): Promise<{ car: Car }> {
    return (await post<{ car }>(`/car/register`, {car}))
}

export async function getCars(userId: string): Promise<Array<Car>> {
    const response = await get<{userId: string, cars: Array<Car>}>(`/car/${userId}`)
    const cars = response.cars
    return cars
}
